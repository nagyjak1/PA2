#ifndef __PROGTEST__

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>
#include <stdexcept>
#include <vector>
#include <array>
#include <cassert>

using namespace std;
#endif /* __PROGTEST__ */

class   CTimeStamp {
        public:
            CTimeStamp ( const int year, const int month, const int day, const int hour, const int min, const int sec );
            bool isInInterval ( const CTimeStamp from, const CTimeStamp to ) const;

        public:
            int m_Year;
            int m_Month;
            int m_Day;
            int m_Hour;
            int m_Min;
            int m_Sec;
        };

class   CContact {
        public:
            CTimeStamp m_Time;
            int m_P1;
            int m_P2;

            CContact ( CTimeStamp time, int number1, int number2 );
            void printContact ( void ) const;
};

class   CEFaceMask {
        public:
            CEFaceMask & addContact ( const CContact & contact );
            vector<int> listContacts ( const int phoneNumber ) const;
            vector<int> listContacts ( const int phoneNumber, const CTimeStamp from, const CTimeStamp to ) const;

        protected:
            vector<CContact> m_List;
        };

//-----------------------------------------------------------------------------------------------------------------------
        CTimeStamp::CTimeStamp ( const int year, const int month, const int day, const int hour, const int min, const int sec )
        : m_Year ( year ),
        m_Month ( month ),
        m_Day ( day ),
        m_Hour ( hour ),
        m_Min ( min ),
        m_Sec ( sec )
        {}
//-----------------------------------------------------------------------------------------------------------------------
bool    CTimeStamp::isInInterval ( const CTimeStamp from, const CTimeStamp to ) const {

            size_t time = m_Year*10000000000+m_Month*100000000+m_Day*1000000+m_Hour*10000+m_Min*100+m_Sec;
            size_t t_from = from.m_Year*10000000000+from.m_Month*100000000+from.m_Day*1000000+from.m_Hour*10000+from.m_Min*100+from.m_Sec;
            size_t t_to = to.m_Year*10000000000+to.m_Month*100000000+to.m_Day*1000000+to.m_Hour*10000+to.m_Min*100+to.m_Sec;
            if (t_from <= time && t_to >= time) return true;
            else return false;

        }

//-----------------------------------------------------------------------------------------------------------------------
        CContact::CContact ( CTimeStamp time, int number1, int number2 )
        :m_Time ( time ),
        m_P1 ( number1 ),
        m_P2 ( number2 )
        {}
//-----------------------------------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------------------------------------
CEFaceMask & CEFaceMask::addContact ( const CContact & x ) {
            if (x . m_P1 == x . m_P2) return *this;
            m_List.push_back( x );
            return  *this;
}
//-----------------------------------------------------------------------------------------------------------------------
vector<int> CEFaceMask::listContacts (int const phoneNumber) const {
    vector<int> listNew;

    for (auto contact : m_List) {
        if (contact.m_P1 == phoneNumber) {
            listNew.push_back(contact.m_P2);
        }
        if (contact.m_P2 == phoneNumber) {
            listNew.push_back(contact.m_P1);
        }
    }
    auto end = listNew.end();
    for (auto it = listNew.begin(); it != end; ++it) {
        end = std::remove(it + 1, end, *it);
    }
    listNew.erase(end, listNew.end());
    return listNew;
}

//-----------------------------------------------------------------------------------------------------------------------
vector<int>    CEFaceMask::listContacts ( int const phoneNumber, const CTimeStamp from, const CTimeStamp to ) const {
    vector<int> listNew;

    for ( auto contact : m_List ) {
            if ( contact . m_P1 == phoneNumber && contact . m_Time . isInInterval( from, to ) ) {
                listNew.push_back(contact.m_P2);
            }
            if ( contact . m_P2 == phoneNumber && contact . m_Time . isInInterval( from, to ) ) {
                listNew.push_back(contact.m_P1);
            }
        }

    auto end = listNew.end();
    for (auto it = listNew.begin(); it != end; ++it) {
        end = std::remove(it + 1, end, *it);
    }
    listNew.erase(end, listNew.end());
    return listNew;
        }

//-----------------------------------------------------------------------------------------------------------------------
#ifndef __PROGTEST__
int main ()
{
    CEFaceMask test;

    test . addContact ( CContact ( CTimeStamp ( 2021, 1, 10, 12, 40, 10 ), 123456789, 999888777 ) );
    test . addContact ( CContact ( CTimeStamp ( 2021, 1, 12, 12, 40, 10 ), 123456789, 111222333 ) )
            . addContact ( CContact ( CTimeStamp ( 2021, 2, 5, 15, 30, 28 ), 999888777, 555000222 ) );
    test . addContact ( CContact ( CTimeStamp ( 2021, 2, 21, 18, 0, 0 ), 123456789, 999888777 ) );
    test . addContact ( CContact ( CTimeStamp ( 2021, 1, 5, 18, 0, 0 ), 123456789, 456456456 ) );
    test . addContact ( CContact ( CTimeStamp ( 2021, 2, 1, 0, 0, 0 ), 123456789, 123456789 ) );
    assert ( test . listContacts ( 123456789 ) == (vector<int> {999888777, 111222333, 456456456}) );
    assert ( test . listContacts ( 999888777 ) == (vector<int> {123456789, 555000222}) );
    assert ( test . listContacts ( 191919191 ) == (vector<int> {}) );
    assert ( test . listContacts ( 123456789, CTimeStamp ( 2021, 1, 5, 18, 0, 0 ), CTimeStamp ( 2021, 2, 21, 18, 0, 0 ) ) == (vector<int> {999888777, 111222333, 456456456}) );
    assert ( test . listContacts ( 123456789, CTimeStamp ( 2021, 1, 5, 18, 0, 1 ), CTimeStamp ( 2021, 2, 21, 17, 59, 59 ) ) == (vector<int> {999888777, 111222333}) );
    assert ( test . listContacts ( 123456789, CTimeStamp ( 2021, 1, 10, 12, 41, 9 ), CTimeStamp ( 2021, 2, 21, 17, 59, 59 ) ) == (vector<int> {111222333}) );
    return 0;
}
#endif /* __PROGTEST__ */
