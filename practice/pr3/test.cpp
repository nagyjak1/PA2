#ifndef __PROGTEST__
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>
#include <stdexcept>
#include <vector>
#include <array>
#include <cassert>
using namespace std;
#endif /* __PROGTEST__ */





struct Record {
	string name;
	string surname;
	string number;
};
// Test gitu

/**
* Tests if string is number, contains only 9 chars and doesnt start with '0'.\
*
* @param s String to test.
* @return True if string is OK, or False if string isnt OK.
*/
bool number_test (const string & s) {
	
    string::const_iterator it = s.begin();
	
	if ( ( * it ) == '0') return false;
	
	if ( s.length() != 9 ) return false; //test
	
    while (it != s.end() && isdigit(*it)) ++it;
	return !s.empty() && it == s.end();
}


bool parseReport ( istream & in, vector <Record> & array ) {
	char ending;
	Record tmp;
	if ( ( in >> skipws >> tmp.name >> tmp.surname >> tmp.number >> ending) ||
				! in . eof () ||
				! number_test ( tmp.number ) ) return false;
	array.push_back( tmp );
	return true;
}
	
bool parseQuestion ( istream & in, string & question ) {
	if ( in >> skipws >> question ) {
		if ( ! in . eof () ) return false;
	}
	return true;
}


bool report ( const string & fileName, ostream & out )
{
	vector <Record> records;
	
	ifstream inFile ( fileName );
	if ( ! inFile.good() ) return false;
	
	string line;
	bool reading_records = true;
	string question;
  
	while ( getline ( inFile, line ) ) {
		istringstream iss ( line );
    
		if ( reading_records && line == "" ) { 	 //if empty line is found, move to phase 2
			reading_records = false;
			continue;
		}
			
		if ( ! reading_records ) {		 //phase 2 -> saving questions
      
			if ( parseQuestion ( iss, question ) ) {
				int found = 0;	
				
				for ( vector<Record>::iterator ite = records.begin(); ite != records.end(); ++ite ) {
					if ( ite->name == question || ite->surname == question ) {
						found++;
						out << ite->name << " " << ite->surname << " " << ite->number << endl;
					}
				}
				out << "-> " << found << endl;
				found = 0;
				continue;
			}
      
			else return false;
		}
    
		if ( parseReport ( iss, records ) )	continue;
		else return false;
	}
  
	return true;
}

#ifndef __PROGTEST__
int main ()
{
  
 report( "tests/test0_in.txt", cout );
  
 return 0;
}
#endif /* __PROGTEST__ */
