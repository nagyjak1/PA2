#ifndef __PROGTEST__
#include <iostream>
#include <iomanip>
#include <string>
#include <stdexcept>
using namespace std;

class CTimeTester;
#endif /* __PROGTEST__ */

class CTime
{
private:
    int m_Hour;
    int m_Minute;
    int m_Second;
    void simplify ();
    bool isValid ();
    long toSeconds () const { return (m_Hour * 60 * 60 + m_Minute * 60 + m_Second) % 86400; }
public:
    // constructor
    CTime ( ) { }
    CTime ( int, int, int );
    // arithmetic operators
    CTime operator + ( const int x ) const;
    CTime operator - ( const int ) const;
    CTime & operator += ( const int x) { m_Second += x; simplify(); return *this;}
    CTime & operator -= ( const int x );
    int operator - ( const CTime ) const;
    CTime & operator ++ ( ) { return *this += 1; }
    CTime & operator -- ( );
    CTime operator ++ ( int );
    CTime operator -- ( int );
    // comparison operators
    bool operator < ( const CTime x ) const { return toSeconds() < x . toSeconds(); }
    bool operator <= ( const CTime x ) const { return toSeconds() <= x . toSeconds(); }
    bool operator > ( const CTime x ) const { return toSeconds() > x . toSeconds(); }
    bool operator >= ( const CTime x ) const { return toSeconds() >= x . toSeconds(); }
    bool operator == ( const CTime x ) const { return toSeconds() == x . toSeconds(); }
    bool operator != ( const CTime x ) const { return toSeconds() != x . toSeconds(); }
    // in/out operators
    friend ostream & operator << ( ostream &, CTime );
    friend istream & operator >> ( istream &, CTime &);

    friend class ::CTimeTester;
};
//------------------------------------------------------------------------------------------------------//
CTime::CTime ( int hours, int minutes, int seconds = 0 ) {
    m_Hour = hours;
    m_Minute = minutes;
    m_Second = seconds;
    if (m_Hour > 23 || m_Hour < 0 || m_Minute < 0 || m_Minute > 59 || m_Second < 0 || m_Second > 59) throw invalid_argument("Invalid arguments");
}
//------------------------------------------------------------------------------------------------------//
void CTime::simplify () {
    while ( m_Second < 0) {
        m_Minute--;
        m_Second += 60;
    }
    while ( m_Minute < 0) {
        m_Hour--;
        m_Minute += 60;
    }
    while ( m_Hour < 0 ) m_Hour += 24;
    m_Minute += (m_Second / 60);
    m_Second = m_Second % 60;
    m_Hour += (m_Minute / 60);
    m_Minute = m_Minute % 60;
    while (m_Hour > 23) m_Hour -= 24;
    return;
}
//------------------------------------------------------------------------------------------------------//
bool CTime::isValid () {
    if (m_Hour > 23 || m_Hour < 0 || m_Minute < 0 || m_Minute > 59 || m_Second < 0 || m_Second > 59) return false;
    else return true;
}
//------------------------------------------------------------------------------------------------------//
CTime CTime::operator + ( const int x ) const {
    CTime y ( *this );
    y . m_Second = m_Second + x;
    y . simplify();
    return y;
}
//------------------------------------------------------------------------------------------------------//
CTime CTime::operator - ( const int x ) const {
    CTime y ( *this );
    y . m_Second = m_Second - x;
    y . simplify();
    return y;
}
//------------------------------------------------------------------------------------------------------//
CTime & CTime::operator -= ( const int x ) {
    m_Second -= x;
    this -> simplify();
    return *this;
}
//------------------------------------------------------------------------------------------------------//
int CTime::operator - ( const CTime t ) const {
    int x = t . toSeconds() % 86400;
    int y = toSeconds() % 86400;
    if ( abs( x - y ) < 43200 ) return abs( x - y);
    if ( abs( x - y ) > 43200 ) {
        if ( x < y ) return x + 86400 - y;
        if ( y < x ) return y + 86400 - x;
    }
    if ( t == *this) return 0;
    else return 43200;
}
//------------------------------------------------------------------------------------------------------//
CTime & CTime::operator -- ( ) {
    m_Second--;
    this -> simplify();
    return *this;
}
//------------------------------------------------------------------------------------------------------//
CTime CTime::operator ++ ( int ) {
    CTime tmp (*this);
    m_Second++;
    simplify();
    return tmp;
}
//------------------------------------------------------------------------------------------------------//
CTime CTime::operator -- ( int ) {
    CTime tmp (*this);
    m_Second--;
    simplify();
    return tmp;
}
//------------------------------------------------------------------------------------------------------//
ostream & operator << ( ostream & os, CTime x ) {
    os << setfill(' ') << setw(2) << x . m_Hour << ":" << setfill('0') << setw(2) << x . m_Minute << ":" << setw(2) << x . m_Second;
    return ( os );
}
//------------------------------------------------------------------------------------------------------//
istream & operator >> ( istream & is, CTime & x) {
    char c1, c2;
    int h, m, s;
    if ( ! ( is >> h >> noskipws >> c1 >> m >> c2 >> s ) || c1 != ':' || c2 != ':'
        || h > 23 || h < 0 || m > 59 || m < 0 || s > 59 || s < 0) {
        is.setstate(ios::failbit);
        return is;
    }
    x . m_Hour = h;
    x . m_Minute = m;
    x . m_Second = s;
    return is;
}
//------------------------------------------------------------------------------------------------------//
CTime operator + ( int x, CTime time ) { return time + x; }
//------------------------------------------------------------------------------------------------------//
CTime operator - ( int x, CTime time ) { return time - x; }
//------------------------------------------------------------------------------------------------------//
#ifndef __PROGTEST__
struct CTimeTester
{
    static bool test ( const CTime & time, int hour, int minute, int second )
    {
        return time.m_Hour == hour 
            && time.m_Minute == minute
            && time.m_Second == second;
    }
};

#include <cassert>
#include <sstream>

int main ()
{
    return 0;
}
#endif /* __PROGTEST__ */
