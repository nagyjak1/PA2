#ifndef __PROGTEST__
#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <memory>
#include <functional>
#include <stdexcept>
#include "ipaddress.h"
using namespace std;
#endif /* __PROGTEST__ */
//----------------------------------------------------------------------------------------------------------------------
class CRec
{
public:
    CRec ( const string name )
    : m_Name ( name ) {}
    //------------------------------------------------------------------------------------------------------------------
    friend ostream & operator << ( ostream & os, const CRec & x ) {
        x . Print( os );
        return os;
    }
    //------------------------------------------------------------------------------------------------------------------
    virtual ~CRec () = default;
    virtual string Name () const { return m_Name; };
    virtual string Type () const = 0;
    virtual void Print ( ostream & os ) const = 0;
    virtual CRec* Clone () const = 0;
    virtual string Address () const = 0;
    virtual int Number () const = 0;
    bool operator == ( const CRec & x ) const { return x.Type() == Type() && x.Number() == Number() &&
                                                        x.Name() == Name() && x.Address() == Address(); }
    //------------------------------------------------------------------------------------------------------------------
protected:
    string m_Name;
};
//----------------------------------------------------------------------------------------------------------------------
class CRecA : public CRec
{
public:
    CRecA ( const string name, const CIPv4 ip )
    : CRec ( name ),
    m_IP ( ip ) {}
    //------------------------------------------------------------------------------------------------------------------
    CRecA ( const CRecA & x )
    : CRec ( x . m_Name ),
    m_IP ( x . m_IP ) {}
    //------------------------------------------------------------------------------------------------------------------
    virtual string Address () const {
        ostringstream strAddress;
        strAddress << m_IP;
        return strAddress.str();
    }
    //------------------------------------------------------------------------------------------------------------------
    virtual string Type () const { return "A"; }
    virtual void Print ( ostream & os ) const { os << m_Name << " A " << m_IP; }
    virtual CRecA * Clone () const { return new CRecA ( *this ); }
    virtual int Number () const { return 0; }
    //------------------------------------------------------------------------------------------------------------------
private:
    CIPv4 m_IP;
};
//----------------------------------------------------------------------------------------------------------------------
class CRecAAAA : public CRec
{
public:
    CRecAAAA ( const string name, const CIPv6 ip )
    : CRec ( name ),
      m_IP ( ip ) {}
    //------------------------------------------------------------------------------------------------------------------
    CRecAAAA ( const CRecAAAA & x )
    : CRec ( x . m_Name ),
    m_IP ( x . m_IP ) {}
    //------------------------------------------------------------------------------------------------------------------
    virtual string Address () const {
        ostringstream strAddress;
        strAddress << m_IP;
        return strAddress.str();
    }
    //------------------------------------------------------------------------------------------------------------------
    virtual string Type () const { return "AAAA"; }
    virtual void Print ( ostream & os ) const { os << m_Name << " AAAA " << m_IP; }
    virtual CRecAAAA * Clone () const { return new CRecAAAA (*this); }
    virtual int Number () const { return 0; }
    //------------------------------------------------------------------------------------------------------------------
private:
    CIPv6 m_IP;
};
//----------------------------------------------------------------------------------------------------------------------
class CRecMX : public CRec
{
public:
    CRecMX ( const string name, const string ip, const int number )
    : CRec ( name ),
      m_IP ( ip ),
      m_Number ( number ) {}
    //------------------------------------------------------------------------------------------------------------------
    CRecMX ( const CRecMX & x )
    : CRec ( x . m_Name ),
    m_IP ( x . m_IP ),
    m_Number ( x . m_Number ) {}
    //------------------------------------------------------------------------------------------------------------------
    virtual string Type () const { return "MX"; }
    virtual void Print ( ostream & os ) const { os << m_Name << " MX " << m_Number << " " << m_IP; }
    virtual CRecMX * Clone () const { return new CRecMX (*this); }
    virtual string Address () const { return m_IP; }
    virtual int Number () const { return m_Number; }
    //------------------------------------------------------------------------------------------------------------------
private:
    string m_IP;
    int m_Number;
};
//----------------------------------------------------------------------------------------------------------------------
class CZone
{
public:
    class CSearchResult {
        public:
        CSearchResult () { }
        size_t Count () const { return m_Results . size(); }
        //----------------------------------------------------------------------------------------------------------
        friend ostream& operator << ( ostream & os, const CSearchResult & x) {
            for ( auto i : x . m_Results ) {
                i -> Print(os);
                os << endl;
            }
            return os;
        }
        //----------------------------------------------------------------------------------------------------------
        CRec & operator [] ( const size_t index ) const {
            if ( index > m_Results . size () )
                throw out_of_range(to_string(index));
            return *(m_Results.at(index));
        }
        //----------------------------------------------------------------------------------------------------------
        vector <CRec*> m_Results;
    };
    //------------------------------------------------------------------------------------------------------------------
    CZone ( const string name )
    : m_Name ( name )
    {
    }
    ~CZone () { for ( auto i : m_IPs ) delete i; }
    //------------------------------------------------------------------------------------------------------------------
    CZone ( const CZone & src )
    : m_Name ( src . m_Name )
    {
        for ( auto i : src . m_IPs )
            m_IPs.push_back ( i -> Clone() );
    }
    //------------------------------------------------------------------------------------------------------------------
    CZone & operator = ( const CZone & src ) {
        if ( this == &src ) return *this;
        m_Name = src . m_Name;
        for ( auto i : m_IPs ) delete i;
        m_IPs . clear();
        for ( auto i : src . m_IPs )
            m_IPs . push_back ( i->Clone() );
        return *this;
    }
    //------------------------------------------------------------------------------------------------------------------
    bool Add ( const CRec & x ) {
        for (auto i : m_IPs) {
            if (*i == x) return false;
        }
            m_IPs.push_back(x.Clone());
            return true;
    }
    //------------------------------------------------------------------------------------------------------------------
    bool Del ( const CRec & x ) {
        for ( auto i = m_IPs.begin(); i < m_IPs.end(); i++ )
            if ( **i == x ) {
                delete *i;
                m_IPs.erase( i );
                return true;
            }
        return false;
    }
    //------------------------------------------------------------------------------------------------------------------
    CSearchResult Search ( const string & name ) const {
        CSearchResult x;
        for ( auto i : m_IPs ) {
            if (i -> Name() == name )
                x . m_Results . push_back ( i );
        }
        return x;
    }
    //------------------------------------------------------------------------------------------------------------------
    void Print ( ostream & os ) const {
        os << m_Name << endl;
        for ( auto i : m_IPs ) {
            if  ( i == m_IPs.back() )
                os << " \\- ";
            else
                os << " +- ";
            i -> Print ( os );
            os << endl;
        }
    }
    //------------------------------------------------------------------------------------------------------------------
    friend ostream & operator << ( ostream & os, const CZone & x ) { x . Print ( os ); return os;}
    //------------------------------------------------------------------------------------------------------------------
private:
    vector < CRec* > m_IPs;
    string m_Name;
};
//----------------------------------------------------------------------------------------------------------------------
#ifndef __PROGTEST__
int main ( void ) {
  return 0;
}
#endif /* __PROGTEST__ */
