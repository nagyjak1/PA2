#ifndef __PROGTEST__
#include <cassert>
#include <string>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <vector>
#include <set>
#include <list>
#include <map>
#include <algorithm>

using namespace std;

class CResult {
public:
    CResult ( const string & name, unsigned int   studentID, const string & test, int result )
      : m_Name ( name ), m_StudentID ( studentID ), m_Test ( test ), m_Result ( result ) { }
    bool operator == ( const CResult& other ) const { return m_Name == other . m_Name && m_StudentID == other . m_StudentID && m_Test == other . m_Test && m_Result == other . m_Result; }
    string m_Name;
    unsigned int m_StudentID;
    string m_Test;
    int m_Result;
 };
#endif /* __PROGTEST__ */
#define NO_GRADE -24000
//----------------------------------------------------------------------------------------------------------------------
class CStudent {
public:
    CStudent() { m_ID = 0; m_Name = ""; cards = vector <string> (); }
    unsigned int m_ID;
    string m_Name;
    vector <string> cards;
};
//----------------------------------------------------------------------------------------------------------------------
class CTest {
public:
    CTest ( string name = "") { m_Name = name; StudentsInTest = set <CStudent> (); }
    string m_Name;
    set <CStudent> StudentsInTest;
};
//----------------------------------------------------------------------------------------------------------------------
class CCard {
public:
    CCard () { m_Owner = nullptr; m_Card = ""; }
    CStudent* m_Owner;
    string m_Card;
};
//----------------------------------------------------------------------------------------------------------------------
bool operator < ( const CStudent & a, const CStudent & b ) { return a . m_ID < b . m_ID; }
//----------------------------------------------------------------------------------------------------------------------
bool operator < ( const CCard a, const CCard b ) { return a . m_Card < b . m_Card; }
//----------------------------------------------------------------------------------------------------------------------
bool operator < ( const CTest a, const CTest b ) { return a . m_Name < b . m_Name; }
//----------------------------------------------------------------------------------------------------------------------
bool operator < ( const CResult a, const CResult b )  { if ( a . m_StudentID == b . m_StudentID ) return a . m_Test < b. m_Test;
                                                        return a . m_StudentID < b . m_StudentID; }
//----------------------------------------------------------------------------------------------------------------------
bool cmpName ( const CResult & a, const CResult & b ) { return a . m_Name < b . m_Name; }
//----------------------------------------------------------------------------------------------------------------------
bool cmpID ( const CResult & a, const CResult & b ) { return a . m_StudentID < b . m_StudentID; }
//----------------------------------------------------------------------------------------------------------------------
bool cmpResult ( const CResult & a, const CResult & b ) { return a . m_Result > b . m_Result; }
//----------------------------------------------------------------------------------------------------------------------
class CExam {
public:
    static const int SORT_NONE   = 0;
    static const int SORT_ID     = 1;
    static const int SORT_NAME   = 2;
    static const int SORT_RESULT = 3;

    CExam() { students = map < unsigned int, CStudent* > (); cards = map < string, CStudent* > (); tests = map < string, vector < CResult> > ();}
    ~CExam() { for ( auto i : students ) delete i . second; }
    bool Load ( istream & cardMap );
    bool Register ( const string & cardID, const string & test );
    bool Assess ( unsigned int studentID, const string & test, int result );
    list<CResult> ListTest ( const string & testName, int sortBy ) const;
    set<unsigned int> ListMissing ( const string & testName ) const;
private:
    map <unsigned int, CStudent*> students;
    map <string, CStudent*> cards;
    map <string, vector<CResult>> tests;
};
//----------------------------------------------------------------------------------------------------------------------
set <unsigned int> CExam::ListMissing ( const string &testName ) const {
    set <unsigned int> result {};
    auto test = tests . find ( testName );
    if ( test == tests . end() ) return result;
    for ( auto student : test -> second ) if ( student . m_Result == NO_GRADE ) result . insert ( student . m_StudentID );
    return result;
}
//----------------------------------------------------------------------------------------------------------------------
list<CResult> CExam::ListTest(const string &testName, int sortBy) const {
    list<CResult> result {};
    auto test = tests . find ( testName );
    if ( test == tests . end() ) return result;
    for ( auto student : test -> second ) if ( student . m_Result != NO_GRADE ) result . push_back ( student );
    reverse ( result . begin(), result . end() );
    switch (sortBy) {
        case 1: result . sort ( cmpID ); break;
        case 2: result . sort ( cmpName ); break;
        case 3: result . sort ( cmpResult ); break;
    }
    return result;
}
//----------------------------------------------------------------------------------------------------------------------
bool CExam::Assess ( unsigned int studentID, const string &test, int result ) {
    auto testsIt = tests . find ( test );
    if ( testsIt == tests . end() ) return false;     // najde zadany test mezi testy
    auto studentsIt = students . find ( studentID );
    if ( studentsIt == students . end() ) return false;    //   mezi studenty najde zadanou studentID a ziskame CStudent*

    for (auto studentInTestIt = testsIt -> second . begin(); studentInTestIt < testsIt -> second . end(); ++studentInTestIt ) {
        if ( studentInTestIt -> m_StudentID == studentsIt -> second -> m_ID ) {
            if ( studentInTestIt -> m_Result != NO_GRADE ) return false;
            studentInTestIt -> m_Result = result;
            CResult tmp = *studentInTestIt;
            testsIt -> second . erase ( studentInTestIt );
            testsIt -> second . insert ( testsIt -> second . begin (), tmp );
            return true;
        }
    }
    return false;
}
//----------------------------------------------------------------------------------------------------------------------
bool CExam::Register ( const string &cardID, const string &test ) {
    auto cardsIt = cards . find ( cardID );
    if ( cardsIt == cards . end() ) return false;     // zadana karta neni jeste v databazi -> return false
    auto testsIt = tests . find ( test );
    CResult newStudent ( cardsIt -> second -> m_Name, cardsIt -> second -> m_ID, test, NO_GRADE );

    if ( testsIt == tests . end() ) {    // zadany test neni jeste v databazi -> vytvorit novy s prvnim studentem
        tests . insert ( pair<string, vector<CResult>> ( test, vector <CResult> { newStudent } ) );
        return true;
    }
    for ( auto student : testsIt -> second ) if ( student == newStudent ) return false;      // zadany student jiz je mezi studenty daneho testu -> return false
    testsIt -> second . push_back ( newStudent );
    return true;
}
//----------------------------------------------------------------------------------------------------------------------
bool CExam::Load ( istream &cardMap ) {
    map <unsigned int, CStudent*> studentsTmp;
    map <string, CStudent*> cardsTmp;
    for ( string line; getline ( cardMap, line ); ) {
        CStudent * newStudent = new CStudent;
        istringstream iss ( line );
        string ID, name, cardStr;
        unsigned int intID;

        getline( iss, ID, ':' );
        intID = stoi ( ID );
        newStudent -> m_ID = intID;
        getline( iss, name, ':' );
        newStudent -> m_Name = name;
        while (getline(iss, cardStr, ',')) {
            cardStr . erase ( remove ( cardStr . begin(), cardStr . end(), ' '), cardStr . end() );
            newStudent -> cards . push_back ( cardStr );
            if ( cards . count ( cardStr ) != 0 || ! cardsTmp . insert ( pair < string, CStudent*> (cardStr , newStudent ) ) . second  ) {
                delete newStudent;
                for ( auto student : studentsTmp ) delete student . second;
                return false;
            }
        }
        if ( students . count ( intID ) != 0 || ! studentsTmp . insert ( pair<unsigned int, CStudent*>(intID, newStudent) ) . second  )  {
            delete newStudent;
            for (auto student : studentsTmp) delete student . second;
            return false;
        }
    }
    students . insert ( studentsTmp . begin(), studentsTmp . end() );
    cards . insert ( cardsTmp . begin(), cardsTmp . end() );
    return true;
}
//----------------------------------------------------------------------------------------------------------------------
#ifndef __PROGTEST__
int main ( void ) { }
#endif /* __PROGTEST__ */