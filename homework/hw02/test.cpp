#ifndef __PROGTEST__
#include <cassert>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <string>
#include <memory>
#include <functional>
#include <vector>
#include <algorithm>
using namespace std;
#endif /* __PROGTEST__ */

struct CPerson {
    string m_Name;
    string m_Surname;
    vector <string> RZs;
    CPerson ( string name, string surname ) { m_Name = name; m_Surname = surname; };
    bool operator == ( const CPerson & x ) const { return ( this -> m_Name == x . m_Name && this -> m_Surname == x . m_Surname ); }
};
//---------------------------------------------------------------------------------------------------------------
struct CCar {
    CCar ( string rz, CPerson *owner = NULL ) { m_RZ = rz; m_Owner = owner; }
    string m_RZ;
    CPerson * m_Owner;
    bool operator < ( const CCar & x ) const { return ( this -> m_RZ < x . m_RZ ); }
};
//---------------------------------------------------------------------------------------------------------------
class CCarList
{
  public:
    CCarList ( void ) { RZs = vector<string> (); }
    CCarList ( vector<string> rz ) { RZs = rz; }
    const string & RZ      ( void ) const { return RZs . back(); }
    bool           AtEnd   ( void ) const { return RZs . end() == RZs . begin(); }
    void           Next    ( void ) { if ( RZs . size() != 0 ) RZs . pop_back(); }
  private:
    vector <string> RZs;
};
//---------------------------------------------------------------------------------------------------------------
class CPersonList
{
  public:
    CPersonList ( void ) { people = vector<CPerson*> (); }
    CPersonList ( vector <CPerson*> x ) { people = x; }
    const string & Name    ( void ) const { return people . back() -> m_Name; }
    const string & Surname ( void ) const { return people . back() -> m_Surname; }
    bool           AtEnd   ( void ) const { return people . begin() == people . end(); }
    void           Next    ( void ) { if ( people . size() != 0 ) people . pop_back(); }
  private:
    vector <CPerson*> people;
};
//---------------------------------------------------------------------------------------------------------------
class cmpOwner {    // // cmp funkce pro lower_bound, argumenty CPerson*, CPerson*
public:
    bool operator() ( CPerson* x , CPerson* y) {
        if ( x -> m_Surname == y -> m_Surname ) return ( x -> m_Name > y -> m_Name );
        return ( x -> m_Surname > y -> m_Surname );
        // pokud je x v abecede za y, vrati funkce true - coz pri pouziti lower_bound znamena, ze x by se mel ulozit pred y
        // C++ reference - lower bound cmpFunction:   "The value returned indicates whether the first argument is considered to go before the second"
    }
};
//---------------------------------------------------------------------------------------------------------------
class cmpPerson {     // cmp funkce pro lower_bound, argumenty CPerson*, CPerson
public:
    bool operator() ( CPerson* x , CPerson y) {
        if ( x -> m_Surname == y . m_Surname ) return ( x -> m_Name > y . m_Name );
        return ( x -> m_Surname > y . m_Surname );
    }
};
//---------------------------------------------------------------------------------------------------------------
class CRegister
{
  public:
                CRegister  ( void );
                ~CRegister  ( void ) { for ( auto &i : people ) delete i; } // rusim instanci objektu CRegister musim uvolnit vector people, ktery ma prvky naalokovane dynamicky
    CRegister  ( const CRegister & src ) = delete;
    CRegister & operator = ( const CRegister & src ) = delete;
    bool        AddCar     ( const string & rz,
                             const string & name,
                             const string & surname );
    bool        DelCar     ( const string & rz );
    bool        Transfer   ( const string & rz,
                             const string & nName,
                             const string & nSurname);
    CCarList    ListCars   ( const string & name,
                             const string & surname ) const;
    int         CountCars  ( const string & name,
                             const string & surname ) const;
    CPersonList ListPersons( void ) const { return CPersonList ( people ); }
private:
    vector <CPerson*> people;
    vector <CCar> cars;

    bool RZUsed ( const string rz ) const { return binary_search ( cars.begin(), cars.end(), CCar ( rz ) ); } // projde vector cars a zjisti zda se zde zadana RZ vyskytuje
    void addRZtoPerson ( CPerson &, const string &);
    void removeRZfromPerson ( CPerson &, const string &);
    void removePerson ( CPerson );
};
//---------------------------------------------------------------------------------------------
// vytvoreni nove instance objektu CRegister s prazdnym vectorem<CPerson*> people a prazdnym vectorem<CCar> cars
CRegister::CRegister() {
    people = vector<CPerson*> ();
    cars = vector<CCar> ();
}
//---------------------------------------------------------------------------------------------
// prida k zadanemu cloveku do vlastnictvi dalsi RZ
void CRegister::addRZtoPerson ( CPerson & x, const string & s ) {
    auto it = lower_bound ( x . RZs . begin(), x . RZs . end(), s );
    x . RZs . insert ( it, s );
}
//---------------------------------------------------------------------------------------------
// vymaze u zadaneho cloveka z vlastnictvi zadanou RZ, pokud nebude nasledne clovek vlastnit zadne auta, vymaze funkce z vectoru people zadaneho cloveka
void CRegister::removeRZfromPerson ( CPerson & x, const string & s) {
    auto it = lower_bound( x .RZs . begin(), x . RZs . end(), s );
    x . RZs . erase ( it );

    if ( x . RZs . size() == 0 ) {   // pokud osoba nema zadna auta, musi byt vymazana z vectoru people
        auto itPeople = lower_bound ( people . begin(), people .end(), x, cmpPerson() );
        delete *itPeople;
        people . erase ( itPeople );
    }
}
//---------------------------------------------------------------------------------------------
bool CRegister::AddCar(const string &rz, const string &name, const string &surname) {
    if ( RZUsed ( rz ) ) return false;   // pokud je RZ jiz pouzivana = chyba - return false

    CPerson* owner = new CPerson ( name, surname );
    CCar car ( rz, owner );

    auto itPeople = lower_bound ( people.begin(), people.end(), owner, cmpOwner() );     // projde vector people a do IT ulozi misto, kam by se mohl ulozit novy clovek ( podle abecedy)
                                                                                                        // funkce cmpOwner() zaridi, ze clovek drive v abecede se vlozi blize ke konci vectoru
    if ( itPeople != people.end() && *(*itPeople) == *owner ) {  // tento clovek uz v databazi je
        addRZtoPerson ( *(* itPeople), rz );      // pridani dalsi RZ do vlastnictvi dane osoby
        car . m_Owner = *itPeople;         //    majitel noveho auta se nastavi na danou osobu ze seznamu
        delete owner;          //    owner se muze smazat, protoze tento clovek uz v seznamu je
    }
    else {     // pridavame noveho cloveka
        people . insert ( itPeople, owner );     // do people se na spravnou pozici it vlozi novy clovek
        owner -> RZs . push_back( rz );     //  novemu cloveku se do jeho seznamu RZ vlozi nova RZ
    }

    auto itCars = lower_bound ( cars.begin(), cars.end(), car );    // najdeme vhodne misto pro vlozeni noveho car
    cars.insert ( itCars, car );     // vlozime nove auto do vectoru cars
    return true;
}
//---------------------------------------------------------------------------------------------
bool CRegister::DelCar ( const string &rz ) {
    CCar car ( rz );
    auto it = lower_bound ( cars . begin(), cars . end(), car );     // ve vectoru cars najdeme zadanou RZ

    if ( it != cars . end() && it -> m_RZ == car . m_RZ ) {     // nasli jsme zadanou RZ na pozici it
        removeRZfromPerson ( *(it -> m_Owner), rz);        // it -> m_Owner je ukazatel na osobu vlastnici zadanou RZ, *(it -> m_Owner) je CPerson vlastnici zadanou RZ
                                                              // removeRZfromPerson zaridi vymazani RZ se seznamu RZ vlastnenych danym clovekem a
                                                              // pokud jiz nevlastni zadna auta, odstrani celou osobu z vectoru people
        cars . erase ( it );                                  // odstranime nalezene auto z vectoru cars

        return true;
    }
    return false;    // jinak pokud ve vectoru cars zadana RZ neexistuje - return false
}
//---------------------------------------------------------------------------------------------
int CRegister::CountCars ( const string &name, const string &surname ) const {
    CPerson* person = new CPerson ( name, surname);

    auto it = lower_bound ( people . begin(), people . end(), person, cmpOwner() );

    if ( it != people . end() && *(*it) == *person) {
        delete person;
        return (*it) -> RZs . size();
    }
    delete person;
    return 0;
}
//---------------------------------------------------------------------------------------------
CCarList CRegister::ListCars ( const string &name, const string &surname ) const {
    CPerson* person = new CPerson ( name, surname );

    auto it = lower_bound( people . begin(), people . end(), person, cmpOwner() );

    if ( it != people . end() && *(*it) == *person) {       // nasli jsme zadaneho cloveka ve vectoru people
        delete person;
        return CCarList ( (*it)->RZs );     // vrati CCarList ( vector<CCar> ) -> konstruktor vytvori vector<CCar> obsahujici RZs zadaneho cloveka
    }
    delete person;      // nenasli jsme zadaneho cloveka ve vectoru people
    return CCarList ();     // vrati CCarList ()  -> konstruktor vytvori prazdny vector<CCar>
}
//---------------------------------------------------------------------------------------------
bool CRegister::Transfer ( const string &rz, const string &nName, const string &nSurname ) {
    if ( ! RZUsed( rz ) ) return false;     // zadana RZ v databazi neexistuje - chyba - return false

    CCar car ( rz );

    auto itCars = lower_bound( cars . begin (), cars . end(), car );        // iterator na misto v cars kde se nachazi zadana RZ

    if ( itCars != cars . end() && itCars -> m_RZ == rz ) {
        CPerson* newOwner = new CPerson ( nName, nSurname);     // CPerson novy vlastnik

        if ( * (itCars -> m_Owner) == *newOwner ) {     // pokud je vlastnik auta stejny jako novy vlastnik - return false
            delete newOwner;
            return false;
        }

        removeRZfromPerson( *(itCars -> m_Owner), rz);      // se vectoru RZs puvodniho vlastnika odstranime zadanou RZ

        auto itPeople = lower_bound ( people . begin(), people . end(), newOwner, cmpOwner() );   // iterator na misto kde by se mel nachazet novy vlastnik

        if ( itPeople != people . end() && *(*itPeople) == *newOwner) {     //mezi people uz je zadany novy vlastnik
            itCars -> m_Owner = *itPeople;      // vlastnik zadane RZ se zmeni na nalezeneho cloveka
            addRZtoPerson ( *(itCars -> m_Owner), rz );     // pridame zadanou RZ do vectoru RZs nalezeneho cloveka
            delete newOwner;
        }
        else {      // mezi people zadany novy vlastnik neni
            itCars -> m_Owner = newOwner;    // vlastnik zadane RZ se zmeni na noveho vlastnika
            people . insert ( itPeople,newOwner );    // noveho vlastnika vlozime na spravne misto do vectoru people
            newOwner -> RZs.push_back( rz );   // novemu vlastniku pridame do jeho vectoru RZs novou RZ
        }
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------------
#ifndef __PROGTEST__

bool checkPerson           ( CRegister    & r,
                             const string & name,
                             const string & surname,
                             vector<string> result )
{
  for ( CCarList l = r . ListCars ( name, surname ); ! l . AtEnd (); l . Next () )
  {
    auto pos = find ( result . begin (), result . end (), l . RZ () ); 
    if ( pos == result . end () )
      return false;
    result . erase ( pos );
  }
  return result . size () == 0;
}

int main ( void )
{
  CRegister b1;
  assert ( b1 . AddCar ( "ABC-12-34", "John", "Smith" ) == true );
  assert ( b1 . AddCar ( "ABC-32-22", "John", "Hacker" ) == true );
  assert ( b1 . AddCar ( "XYZ-11-22", "Peter", "Smith" ) == true );
  assert ( b1 . CountCars ( "John", "Hacker" ) == 1 );
            assert ( checkPerson ( b1, "John", "Hacker", { "ABC-32-22" } ) );
  assert ( b1 . Transfer ( "XYZ-11-22", "John", "Hacker" ) == true );
  assert ( b1 . AddCar ( "XYZ-99-88", "John", "Smith" ) == true );
  assert ( b1 . CountCars ( "John", "Smith" ) == 2 );
            assert ( checkPerson ( b1, "John", "Smith", { "ABC-12-34", "XYZ-99-88" } ) );
  assert ( b1 . CountCars ( "John", "Hacker" ) == 2 );
            assert ( checkPerson ( b1, "John", "Hacker", { "ABC-32-22", "XYZ-11-22" } ) );
  assert ( b1 . CountCars ( "Peter", "Smith" ) == 0 );
  assert ( checkPerson ( b1, "Peter", "Smith", {  } ) );
  assert ( b1 . Transfer ( "XYZ-11-22", "Jane", "Black" ) == true );
  assert ( b1 . CountCars ( "Jane", "Black" ) == 1 );
  assert ( checkPerson ( b1, "Jane", "Black", { "XYZ-11-22" } ) );
  assert ( b1 . CountCars ( "John", "Smith" ) == 2 );
  assert ( checkPerson ( b1, "John", "Smith", { "ABC-12-34", "XYZ-99-88" } ) );
  assert ( b1 . DelCar ( "XYZ-11-22" ) == true );
  assert ( b1 . CountCars ( "Jane", "Black" ) == 0 );
  assert ( checkPerson ( b1, "Jane", "Black", {  } ) );
  assert ( b1 . AddCar ( "XYZ-11-22", "George", "White" ) == true );
  CPersonList i1 = b1 . ListPersons ();
  assert ( ! i1 . AtEnd () && i1 . Surname () == "Hacker" && i1 . Name () == "John" );
  assert ( checkPerson ( b1, "John", "Hacker", { "ABC-32-22" } ) );
  i1 . Next ();
  assert ( ! i1 . AtEnd () && i1 . Surname () == "Smith" && i1 . Name () == "John" );
  assert ( checkPerson ( b1, "John", "Smith", { "ABC-12-34", "XYZ-99-88" } ) );
  i1 . Next ();
  assert ( ! i1 . AtEnd () && i1 . Surname () == "White" && i1 . Name () == "George" );
  assert ( checkPerson ( b1, "George", "White", { "XYZ-11-22" } ) );
  i1 . Next ();
  assert ( i1 . AtEnd () );

  CRegister b2;
  assert ( b2 . AddCar ( "ABC-12-34", "John", "Smith" ) == true );
  assert ( b2 . AddCar ( "ABC-32-22", "John", "Hacker" ) == true );
  assert ( b2 . AddCar ( "XYZ-11-22", "Peter", "Smith" ) == true );
  assert ( b2 . AddCar ( "XYZ-11-22", "Jane", "Black" ) == false );
  assert ( b2 . DelCar ( "AAA-11-11" ) == false );
  assert ( b2 . Transfer ( "BBB-99-99", "John", "Smith" ) == false );
  assert ( b2 . Transfer ( "ABC-12-34", "John", "Smith" ) == false );
  assert ( b2 . CountCars ( "George", "White" ) == 0 );
  assert ( checkPerson ( b2, "George", "White", {  } ) );
  CPersonList i2 = b2 . ListPersons ();
  assert ( ! i2 . AtEnd () && i2 . Surname () == "Hacker" && i2 . Name () == "John" );
  assert ( checkPerson ( b2, "John", "Hacker", { "ABC-32-22" } ) );
  i2 . Next ();
  assert ( ! i2 . AtEnd () && i2 . Surname () == "Smith" && i2 . Name () == "John" );
  assert ( checkPerson ( b2, "John", "Smith", { "ABC-12-34" } ) );
  i2 . Next ();
  assert ( ! i2 . AtEnd () && i2 . Surname () == "Smith" && i2 . Name () == "Peter" );
  assert ( checkPerson ( b2, "Peter", "Smith", { "XYZ-11-22" } ) );
  i2 . Next ();
  assert ( i2 . AtEnd () );


  return 0;
}
#endif /* __PROGTEST__ */
