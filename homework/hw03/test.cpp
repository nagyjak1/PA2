/**
 * Implementation of class CPolynomial to represent real polynomials.
 * Implementation of few methods to work with the polynomials.
 *
 * In the implementation the polynomial is represented by vector of its terms.
 * And every term is represented by struct which contains its degree and its coefficient.
 */

#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <climits>
#include <cmath>
#include <cfloat>
#include <cassert>
#include <unistd.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>
#if defined ( __cplusplus ) && __cplusplus > 199711L /* C++ 11 */
#include <memory>
#endif /* C++ 11 */
using namespace std;
#endif /* __PROGTEST__ */

class CPolynomial
{
public:
    CPolynomial ( );

    /// Assignment operator
    CPolynomial & operator = ( const CPolynomial & );

    /// Arithmetic operations with polynomials
    CPolynomial operator + ( const CPolynomial & ) const;
    CPolynomial operator - ( const CPolynomial & ) const;
    CPolynomial operator * ( const CPolynomial & ) const;
    CPolynomial operator * ( const double ) const;

    /// Equality or inequality of polynomials
    bool operator == ( const CPolynomial & ) const;
    bool operator != ( const CPolynomial & ) const;

    /// Coefficient at term with given degree
    double & operator [] ( const size_t );
    double operator [] ( const size_t ) const;

    /// Value of polynomial for given number
    double operator () ( const double ) const;

    /// Degree of polynomial
    size_t Degree () const;

    /// Print polynomial
    friend ostream & operator << ( ostream &, const CPolynomial & );

private:
    struct CPolynomialTerm {
        double m_Value;
        size_t m_Degree;
    };
    vector <CPolynomialTerm> polynomial;
};
//----------------------------------------------------------------------------------------------------------------------
/**
 * Construct zero value polynomial
 *
 * Insert first element in its vector of its terms.
 * The inserted element's degree and value are both equal zero.
 */
CPolynomial::CPolynomial () {
    CPolynomialTerm x;
    x . m_Degree = 0;
    x . m_Value = 0;
    polynomial . push_back ( x );
}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Set polynomial equal to the given polynomial
 *
 * @params source polynomial to be assigned
 * @return polynomial equal to source
 */
CPolynomial & CPolynomial::operator = ( const CPolynomial & source ) {
    if ( *this == source ) return *this;
    polynomial = source . polynomial;
    return *this;
}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Add two polynomials
 *
 * Adds together coefficients at terms with the same degree.
 *
 * @param right the polynomial to be added
 * @return new polynomial equal to the result of the addition
 */
CPolynomial CPolynomial::operator + ( const CPolynomial & right) const {
    CPolynomial result;

    // If the first polynomial's degree is greater or equal to the second polynomial, the result will have the same
    // degree as the first polynomial. All we have to do is go through the first polynomial and add together the same terms
    // of the two polynomials
    if ( this -> Degree() >= right . Degree() ) {
        result = *this;
        for (size_t idx = 0; idx <= right . Degree(); ++idx)
            result.polynomial[idx].m_Value += right.polynomial[idx].m_Value;
    }

    // Else the result will have the same degree as the second polynomial. We have to go through the seconds polynomial
    // and again add together the same terms of the two polynomials.
    else {
        result = right;
        for ( size_t idx = 0; idx <= this -> Degree(); ++idx )
            result . polynomial[idx] . m_Value += this -> polynomial[idx] . m_Value;
    }
    return result;
}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Subtract two polynomials
 *
 * Subtracts coefficients at terms with the same degree.
 *
 * @param right the polynomial to be subtracted
 * @return new polynomial equal to the result of the subtraction
 */
CPolynomial CPolynomial::operator - ( const CPolynomial & right) const {
    CPolynomial result = *this;

    // If the first polynomial's degree is lower than the second polynomial's degree, we have to resize the first polynomial
    // and add new terms with zero coefficient to the first polynomial. Else we couldn't subtract the two polynomials.
    if ( this -> Degree() < right . Degree() ) {
        size_t oldSize = this -> Degree();
        // Resize to the second's polynomial degree and add 1 for the 0 degree term
        result . polynomial . resize ( right . Degree() + 1 );
        // Set degrees and values in the newly added terms
        for (auto i = oldSize + 1; i <= right . Degree(); ++i) {
            result . polynomial . at(i) . m_Degree = i;
            result . polynomial . at(i) . m_Value = 0;
        }
    }

    // Else the first polynomial's degree is greater or equal to the second's polynomial degree, so we don't have to resize anything.
    // Now we have the first polynomial's degree greater or equal to the second polynomial's degree, so we just go through the second
    // polynomial and subtract its every term from the term of same degree in the first polynomial.
    for ( size_t idx = 0; idx <= right.Degree(); ++idx )
        result . polynomial[idx] . m_Value -= right . polynomial[idx] . m_Value;
    return result;
}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Multiply two polynomials
 *
 * Multiply every term of the first polynomial with every term of the second polynomial.
 *
 * @param right polynomial to multiply with
 * @return new polynomial equal to the result of the multiplication
 */
CPolynomial CPolynomial::operator * ( const CPolynomial & right) const {

    // We have to prepare polynomial to represent the result. The size of its vector will be sum of the degrees of both
    // polynomials plus 1 for the zero degree term. Then we set degrees and values in the newly created polynomial.
    CPolynomial result;
    result . polynomial . resize ( this -> Degree() + right . Degree() + 1 );
    for ( size_t i = 0; i < result . polynomial . size(); ++i ) {
        result . polynomial[i] . m_Degree = i;
        result . polynomial[i] . m_Value = 0;
    }

    // Now we multiply both polynomials using 2 for cycles.
    for ( size_t j = 0; j <= right . Degree(); ++j ) {
        for ( size_t k = 0; k <= this -> Degree(); ++k )
            result . polynomial[k+j] . m_Value += this -> polynomial[k] . m_Value * right . polynomial[j] . m_Value;
    }
    return result;
}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Multiply polynomial and number
 *
 * Multiply every term of the polynomial with given number.
 *
 * @param multiplier number to multiply with the polynomial
 * @return new polynomial equal to the result of the multiplication
 */
CPolynomial CPolynomial::operator * ( const double multiplier ) const {

    // We just go through the polynomial and multiply every term's coefficient by the given multiplier.
    CPolynomial tmp = *this;
    for (auto & y : tmp . polynomial )
        y . m_Value *= multiplier;
    return tmp;
}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Get coefficient at term with given degree
 *
 * @param termDegree degree of term which coefficient to get
 * @return coefficient at the term with given degree
 */
double CPolynomial::operator [] ( const size_t termDegree) const {

    // If the given degree is greater than polynomial's degree, the given term's coefficient must be zero.
    if ( termDegree > Degree() ) return 0;

    // Else we just return the coefficient of the term at the given position in the polynomial's vector.
    return polynomial . at ( termDegree ) . m_Value;
}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Get reference to coefficient at term with given degree
 *
 * The coefficient can be then modified.
 *
 * @param termDegree degree of term which coefficient to get
 * @return reference to coefficient at the term with given degree
 */
double & CPolynomial::operator[] ( const size_t termDegree ) {

    // If the given degree is greater than the polynomial's degree, we have to resize the term's vector because
    // the coefficient can be modified by the user.
    if ( termDegree > Degree () ) {
        size_t oldSize = polynomial . size ();
        polynomial . resize ( termDegree + 1 );

        for ( auto i = oldSize; i < termDegree + 1; i++ ) {
            polynomial . at ( i ) . m_Degree = i;
            polynomial . at ( i ) . m_Value = 0;
        }
    }
    // Now we can safely return the reference to coefficient of the term at the given position in the polynomial's vector.
    return polynomial . at ( termDegree ) . m_Value;
}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Compute value of polynomial for given number
 *
 * @param x number to be computed with
 * @return value of polynomial
 */
double CPolynomial::operator () ( const double x ) const {

    // We just go through the polynomial and for every term we accumulate x to the power of the term's degree.
    double sum = 0;
    for ( auto y : this -> polynomial )
        sum += y . m_Value * pow( x , y . m_Degree );
    return sum;
}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Find the degree of polynomial
 *
 * The degree of polynomial is the highest of the degrees of the
 * polynomial's terms with non-zero coefficients.
 *
 * @return degree of the polynomial
 */
size_t CPolynomial::Degree () const {

    // Because during arithmetic operations sometimes the term's coefficient can become zero, we can't just return the
    // size of the term's vector. We have to go from the end of the vector and find first nonzero coefficient, the
    // first found one's degree is equal to the polynomial's degree.
    for (auto it = polynomial . end() - 1; it != polynomial . begin(); it--)
        if ( it -> m_Value != 0) return it -> m_Degree;

    // Else all the coefficients are zeros, the polynomial's degree is zero as well.
    return 0;
}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Check if two polynomials are equal
 *
 * Two polynomials are equal if their degrees are equal and if
 * all their coefficients at terms with same degree are equal.
 *
 * @param right polynomial to be compared to
 * @return true if polynomials are equal, or false if not
 */
bool CPolynomial::operator == ( const CPolynomial & right ) const {

    // If the degrees do not match, the polynomials are not equal.
    if ( this -> Degree()  != right . Degree() ) return false;

    // Else we have to go through the first polynomial and compare it's coefficients with the second polynomial coefficients.
    for ( size_t idx = 0; idx <= right . Degree(); ++idx )

        // If the coefficients do not match, the polynomials are not equal.
        if ( right . polynomial[idx] . m_Value != this -> polynomial[idx] . m_Value ) return false;

    // Else all coefficients match, so the polynomials are equal.
    return true;
}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Check if two polynomials are not equal
 *
 * @param right polynomial to be compared to
 * @return true if polynomials are not equal, or false if are
 */
bool CPolynomial::operator != ( const CPolynomial & right ) const {
    return ! ( *this == right );
}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Print polynomial
 *
 * Prints from the term with highest degree to the term with lowest degree.
 * Terms with zero coefficients are not printed, but polynomial with all zero coefficients is printed as "0".
 * Terms with coefficients equal to 1 are printed without the coefficients.
 * Term with degree equal to 0 is printed only as its coefficient.
 * "+" at the first printed term is not printed.
 *
 * e.g. 4x^3 + 2x^1 - 5
 *
 * @param os ostream to be printed in
 * @param x polynomial to be printed
 * @return ostream with printed polynomial
 */
ostream & operator << ( ostream & os, const CPolynomial & x ) {
    auto it = x . polynomial . rbegin ();

    // Skip terms with zero coefficient.
    while ( it -> m_Value == 0 ) {
        it++;
        // If all the coefficients are zero, print 0.
        if ( it == x . polynomial . rend() ) return os << 0;
    }

    // ** Here we handle the first term with non zero coefficient **
    // If the first non zero is the last one, we just print it.
    if ( it -> m_Degree == 0) return os << (it -> m_Value >= 0 ? "" : "- ") << abs(it -> m_Value);

        // Here we handle the coefficient equal to 1 or -1, which must be printed without the "1"
    else if ( it -> m_Value == 1 ) { os << "x^" << it -> m_Degree;}
    else if ( it -> m_Value == -1 ) { os << "- x^" << it -> m_Degree;}

        // Here we handle the rest. If the coefficient is negative, we print "-", else we print "". Then we print the rest of the term.
    else os << (it -> m_Value >= 0 ? "":"- ") << abs ( it -> m_Value ) << "*x^" << it -> m_Degree;

    // ** Here we handle the rest of terms **
    // Now when we've printed the first term, the rest of terms will all have same pattern. We go through the term's vector until the end.
    for (it++; it != x . polynomial . rend(); ++it) {

        // We skip zero coefficient terms.
        if ( it -> m_Value == 0 ) continue;

        os << " ";
        // We print the last term.
        if ( it -> m_Degree == 0) return os << (it -> m_Value >= 0 ? "+ " : "- ") << abs(it -> m_Value);

        // Again we handle the coefficients equal to 1 or -1 same way as at the first nonzero term. But here we have to print "+" instead of "".
        if ( it -> m_Value == 1 ) { os << "+ x^" << it -> m_Degree; continue;}
        if ( it -> m_Value == -1 ) { os << "- x^" << it -> m_Degree; continue;}

        // Here we handle the rest again, but again we have to print "+" instead of "" printed at the first nonzero term.
        os << (it -> m_Value >= 0 ? "+ ":"- ") << abs ( it -> m_Value ) << "*x^" << it -> m_Degree;
    }
    return os;
}
//----------------------------------------------------------------------------------------------------------------------
#ifndef __PROGTEST__
int main ( void ) { return 0; }
#endif /* __PROGTEST__ */
//----------------------------------------------------------------------------------------------------------------------