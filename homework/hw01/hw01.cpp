//
// Created by jakub on 16.03.21.
//

#ifndef __PROGTEST__
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstdint>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>
#include <math.h>
using namespace std;

const uint32_t BIG_ENDIAN_ID    = 0x4d6d6d4d;
const uint32_t LITTLE_ENDIAN_ID = 0x49696949;
const uint32_t FRAME_ID         = 0x46727246;
const uint32_t VIDEO_DATA_ID    = 0x56696956;
const uint32_t AUDIO_LIST_ID    = 0x416c6c41;
const uint32_t AUDIO_DATA_ID    = 0x41757541;
#endif /* __PROGTEST__ */

u_int32_t swap (uint32_t num) {
    return ((num & 0xff000000) >> 24) | ((num & 0x00ff0000) >> 8) | ((num & 0x0000ff00) << 8) | (num << 24); // byte 0 to byte 3
}

typedef struct globalHeader {
    uint32_t globalID;
    uint32_t frames;
    uint32_t payload;
} globalHeader;

typedef struct frameHeader {
    uint32_t frameID;
    uint32_t framePayload;
} frameHeader;

typedef struct videoData {
    uint32_t videoID;
    uint32_t videoPayload;

} videoData;

typedef struct audioList {
    uint32_t audioListID;
    uint32_t audioNumber;
    uint32_t audioListPayload;
} audioList;

typedef struct audioData {
    uint32_t audioDataID;
    char audioLang [2];
    uint32_t audioDataPayload;
} audioData;

bool  filterFile          ( const char      * srcFileName,
                            const char      * dstFileName,
                            const char      * lang ) {
    ifstream ifs (srcFileName, ios::in | ios::binary);
    ofstream ofs (dstFileName, ios::out | ios::binary);

    if (! ifs . is_open() ) return false;
    if (! ofs . is_open() ) return false;

    if ( ifs . fail () ) return false;
    if ( ofs . fail () ) return false;

    char zero = 0;

    globalHeader globalHead;

    ifs . read ((char *) &globalHead, sizeof(globalHeader));
    bool littleEndian = false;
    bool bigEndian = false;

    auto globalPayloadPosition = (int)ifs . tellg() - sizeof(uint32_t);
    uint32_t deletedBytes = 0;

    if (globalHead.globalID == LITTLE_ENDIAN_ID) littleEndian = true;
    if (globalHead.globalID == BIG_ENDIAN_ID) bigEndian = true;
    if (!littleEndian && !bigEndian) return false;

    ofs.write( (char*) &globalHead, sizeof(globalHead) );

    if (bigEndian) globalHead.frames = swap(globalHead.frames);

    uint32_t globalBytes = 0;

    frameHeader frameHead;
    for (uint32_t i = 0; i < globalHead.frames; i++) {

        uint32_t deletedBytesInFrame = 0;
        uint32_t frameBytes = 0;

        ifs.read((char *) &frameHead, sizeof(frameHeader));
        if ( frameHead.frameID != FRAME_ID) return false;
        ofs.write((char *) &frameHead, sizeof(frameHeader));

        globalBytes += 8;

        auto framePayloadPosition = (int)ofs . tellp() - sizeof(uint32_t);

        videoData videoData1;
        ifs.read((char *) &videoData1, sizeof(videoData));
        if ( videoData1.videoID != VIDEO_DATA_ID) return false;
        ofs.write((char *) &videoData1, sizeof(videoData));

        globalBytes += 8;
        frameBytes += 8;
        uint32_t videoBytes = 0;

        if (bigEndian) videoData1.videoPayload = swap(videoData1.videoPayload);

        char byte;
        for (uint32_t j = 0; j < videoData1.videoPayload; j++) {
            ifs.read(&byte, sizeof(byte));
            ofs.write(&byte, sizeof(byte));
            globalBytes++;
            frameBytes++;
            videoBytes++;
        }
        if ( videoBytes != videoData1.videoPayload ) {
            return false;
        }

        ifs.ignore(sizeof(uint32_t));
        for (auto k = 0; k < 4; k++) ofs.write(&zero, sizeof(char));   // video CRC
        globalBytes += 4;
        frameBytes += 4;


        audioList audioList1;
        ifs.read((char *) &audioList1, sizeof(audioList));
        if (AUDIO_LIST_ID != audioList1.audioListID) return false;
        ofs.write((char *) &audioList1, sizeof(audioList));

        globalBytes += 12;
        frameBytes += 12;
        uint32_t audioListBytes = 0;

        auto audioListNumberPosition = (int)ofs . tellp() - sizeof(uint32_t) - sizeof(uint32_t);
        auto audioListPayloadPosition = (int)ofs . tellp() - sizeof(uint32_t);
        uint32_t languageNumber = 0;

        if (bigEndian) audioList1.audioNumber = swap( audioList1.audioNumber );

        for (uint32_t l = 0; l < audioList1.audioNumber; l++) {
            audioData audioData1;

            ifs . read ( (char*) &audioData1.audioDataID, sizeof( audioData1.audioDataID ) );
            ifs . read ( audioData1.audioLang, sizeof( audioData1.audioLang ) );
            ifs . read ( (char*) &audioData1.audioDataPayload, sizeof( audioData1.audioDataPayload ) );

            globalBytes += 10;
            frameBytes += 10;
            audioListBytes += 10;
            uint32_t audioDataBytes = 0;

            if (AUDIO_DATA_ID != audioData1.audioDataID) return false;

            if ( ( audioData1.audioLang[0] == lang[0]) && (audioData1.audioLang[1] == lang[1]) )  {

                languageNumber++;
                ofs . write ( (char*) &audioData1.audioDataID, sizeof( audioData1.audioDataID ) );
                ofs . write ( audioData1.audioLang, sizeof( audioData1.audioLang ) );
                ofs . write ( (char*) &audioData1.audioDataPayload, sizeof( audioData1.audioDataPayload ) );

                if (bigEndian) audioData1.audioDataPayload = swap( audioData1.audioDataPayload );

                for (uint32_t m = 0; m < audioData1.audioDataPayload; m++) {
                    ifs . read ( &byte, sizeof(byte) );
                    ofs . write ( &byte, sizeof(byte) );
                    globalBytes++;
                    frameBytes++;
                    audioListBytes++;
                    audioDataBytes++;
                }
                ifs . ignore (sizeof(uint32_t));
                for (auto k = 0; k < 4; k++) ofs.write(&zero, sizeof(char)); // audio data CRC
                globalBytes += 4;\
                frameBytes += 4;
                audioListBytes +=4;
            }

            else {
                if (bigEndian) audioData1.audioDataPayload = swap( audioData1.audioDataPayload );

                ifs.ignore(audioData1.audioDataPayload * sizeof(char));
                deletedBytes += 14;
                deletedBytes += audioData1.audioDataPayload * sizeof(char);
                ifs.ignore(sizeof(uint32_t));

                globalBytes += audioData1.audioDataPayload + 4; // audioData CRC
                frameBytes += audioData1.audioDataPayload;
                frameBytes += 4;
                audioListBytes += audioData1.audioDataPayload + 4;
                audioDataBytes += audioData1.audioDataPayload;

                deletedBytesInFrame += 14;
                deletedBytesInFrame += audioData1.audioDataPayload * sizeof(char);

                if ( (audioDataBytes != audioData1.audioDataPayload) ) {
                    return false;
                }

            }
        }
        if (bigEndian) audioList1.audioListPayload = swap( audioList1.audioListPayload );
        if (audioListBytes != audioList1.audioListPayload) {
            return false;
        }

        if (bigEndian) {
            audioList1.audioNumber = swap(languageNumber);
            audioList1.audioListPayload = swap(audioList1.audioListPayload - deletedBytesInFrame);
        }
        else {
        audioList1.audioNumber = languageNumber;
        audioList1.audioListPayload = audioList1.audioListPayload - deletedBytesInFrame;
        }

        auto tmp = ofs . tellp();
        ofs . seekp ( audioListNumberPosition );
        ofs . write ( (char*) &audioList1.audioNumber, sizeof(uint32_t));
        ofs . seekp ( audioListPayloadPosition );
        ofs . write ( (char*) &audioList1.audioListPayload, sizeof(uint32_t));
        ofs . seekp ( tmp );

        ifs . ignore (sizeof(uint32_t));
        for (auto k = 0; k < 4; k++) ofs.write(&zero, sizeof(char));  // audio list CRC
        globalBytes += 4;
        frameBytes += 4;
        ifs . ignore (sizeof(uint32_t));
        for (auto k = 0; k < 4; k++) ofs.write(&zero, sizeof(char));  // frame CRC
        globalBytes += 4;

        if (bigEndian) frameHead.framePayload = swap ( frameHead.framePayload );

        if (frameBytes != frameHead.framePayload) {
            return false;
        }

        frameHead . framePayload -= deletedBytesInFrame;

        if (bigEndian) frameHead.framePayload = swap ( frameHead.framePayload );

        tmp = ofs . tellp();
        ofs . seekp ( framePayloadPosition );
        ofs . write ( (char*) &frameHead . framePayload, sizeof(uint32_t));
        ofs . seekp ( tmp );

    }
    if (bigEndian) globalHead.payload = swap ( globalHead.payload );
    if (globalBytes != globalHead.payload) {
        return false;
    }
    ifs . ignore (sizeof(uint32_t));
    for (auto k = 0; k < 4; k++) ofs.write(&zero, sizeof(char));    // global CRC/

    globalHead . payload = globalHead . payload - deletedBytes;
    if (bigEndian) globalHead.payload = swap ( globalHead.payload );
    auto tmp = ofs . tellp();
    ofs . seekp ( globalPayloadPosition );
    ofs . write ( (char*) &globalHead.payload, sizeof(uint32_t));
    ofs . seekp ( tmp );

    if ( ! ifs. eof() ) return false;

    if ( ifs . fail() ) return false;
    if ( ofs . fail() ) return false;

    ifs . close();
    ofs . close();

    ifstream inFile( srcFileName, ios::binary | ios::ate);
    ifstream outFile( dstFileName, ios::binary | ios::ate);
    if ( inFile.tellg()  <  outFile.tellg() ) return false;
    inFile.close();
    outFile.close();

    return true;
}

int main ( void ) {
    if  (! filterFile ("sample/big-endian/in_0005.in","sample/out.txt","cs" ) ) cout << "CHYBA" << endl;
    return 0;
}
