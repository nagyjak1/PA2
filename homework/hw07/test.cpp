#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <cctype>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <set>
#include <list>
#include <forward_list>
#include <map>
#include <vector>
#include <string>
#include <algorithm>
#include <functional>
#include <memory>
#include <stdexcept>
using namespace std;
#endif /* __PROGTEST__ */


template <typename T_, typename Cmp_ = less<T_> >
class CIntervalMin
{
  public:
    using const_iterator = typename vector<T_>::const_iterator;
    //------------------------------------------------------------------------------------------------------------------
    CIntervalMin ( const Cmp_ comparator = Cmp_{} )
    : m_Cmp ( comparator ),
    m_List { },
    m_RMQList { },
    m_BLK ( 0 )
    {
    }
    //------------------------------------------------------------------------------------------------------------------
    template <typename Iterator>
    CIntervalMin( const Iterator begin, const Iterator end, const Cmp_ comparator = Cmp_{}  )
    : m_Cmp ( comparator ),
    m_List ( begin, end ),
    m_RMQList { },
    m_BLK ( ceil ( sqrt ( m_List . size () ) ) )
    {
        updateTmp();
    }
    //------------------------------------------------------------------------------------------------------------------
    void push_back ( const T_ & val ) {

        m_List . push_back ( val );

        m_RMQList . push_back ( val );

        if ( m_RMQList . size() > m_BLK * 2 )
            updateTmp();
    }
    //------------------------------------------------------------------------------------------------------------------
    void pop_back ( ) {
        if ( m_List . empty() ) return;

        if ( ! ( m_List . back() < m_RMQList . back() || m_RMQList . back() < m_List . back() ) )
            m_RMQList . pop_back();

        m_List . pop_back();
    }
    //------------------------------------------------------------------------------------------------------------------
    T_ min ( const_iterator start, const_iterator end ) const {
        if ( start == end ) throw invalid_argument ( " invalid argument ");


        size_t i = distance ( CIntervalMin::begin(), start );
        size_t iB = i / m_BLK;

        size_t j = distance ( CIntervalMin::begin(), end );
        size_t jB = j / m_BLK;

        if ( jB == iB )
            return *min_element ( m_List.begin() + i, m_List.begin() + j, m_Cmp);

        T_ m = *min_element( m_List . begin() + i, m_List . begin() + m_BLK * ( iB + 1 ), m_Cmp);

        if ( jB > iB + 1 )
            m = std::min( m, *min_element( m_RMQList.begin() + iB + 1, m_RMQList . begin() + jB, m_Cmp ), m_Cmp );

        if ( j > m_BLK * ( jB ) )
            m = std::min( m, *min_element ( m_List.begin() + m_BLK * jB, m_List.begin() + j, m_Cmp ), m_Cmp );

        return m;
    }
    //------------------------------------------------------------------------------------------------------------------
    const_iterator begin () const { return m_List . begin(); }
    //------------------------------------------------------------------------------------------------------------------
    const_iterator end () const { return m_List . end(); }
    //------------------------------------------------------------------------------------------------------------------
    size_t size () const { return m_List . size (); }
    //------------------------------------------------------------------------------------------------------------------
    void updateTmp () {
        m_BLK = ceil ( sqrt ( m_List . size () ) );
        T_ m = *m_List . begin();
        m_RMQList . clear();

        for ( size_t i = 0; i < m_List . size (); i++ )
            if ( i % m_BLK == 0 ) {
                if ( i > 0 )
                    m_RMQList . push_back (m);
                m = m_List . at(i);
            } else
                m = std::min ( m_List . at(i), m, m_Cmp );
        m_RMQList . push_back(m);
    }
    //------------------------------------------------------------------------------------------------------------------
private:
    const Cmp_ m_Cmp;
    vector<T_> m_List;
    vector<T_> m_RMQList;
    size_t m_BLK;
};

#ifndef __PROGTEST__
//-------------------------------------------------------------------------------------------------
class CStrComparator
{
  public:
               CStrComparator ( bool byLength = true ) 
      : m_ByLength ( byLength ) 
    { 
    }
    bool       operator () ( const string & a, const string & b ) const 
    { 
      return m_ByLength ? a . length () < b . length () : a < b;
    }
  private:
    bool       m_ByLength;  
};
//-------------------------------------------------------------------------------------------------
bool strCaseCmpFn ( const string & a, const string & b )
{
  return strcasecmp ( a . c_str (), b . c_str () ) < 0;
}
//-------------------------------------------------------------------------------------------------

int main ( void )
{
    CIntervalMin <int> a1;
  for ( auto x : initializer_list<int> { 5, 15, 79, 62, -3, 0, 92, 16, 2, -4 } )
    a1 . push_back ( x );

  assert ( a1 . size () == 10 );
  
  ostringstream oss;
  for ( auto x : a1 )
    oss << x << ' ';
  
  assert ( oss . str () == "5 15 79 62 -3 0 92 16 2 -4 " );

  assert ( a1 . min ( a1 . begin (), a1 . end () ) == -4 );
  assert ( a1 . min ( a1 . begin () + 2, a1 . begin () + 3 ) == 79 );
  assert ( a1 . min ( a1 . begin () + 2, a1 . begin () + 9 ) == -3 );

  try
  {
    a1 . min ( a1 . begin (), a1 . begin () );
    assert ( "Missing an exception" == nullptr );
  }
  catch ( const invalid_argument & e )
  {
  }
  catch ( ... )
  {
    assert ( "Invalid exception" == nullptr );
  }
  
  a1 . pop_back ();
  assert ( a1 . size () == 9 );
  a1 . push_back ( 42 );

  assert ( a1 . min ( a1 . begin (), a1 . end () ) == -3 );

  vector<string> words{ "auto", "if", "void", "NULL" };
  CIntervalMin <string> a2 ( words . begin (), words . end () );

  assert ( a2 . min ( a2 . begin (), a2 . end () ) ==  "NULL" );

  CIntervalMin <string, bool(*)(const string &, const string &)> a3 ( words . begin (), words . end (), strCaseCmpFn );
  assert ( a3 . min ( a3 . begin (), a3 . end () ) == "auto" );

  CIntervalMin <string, CStrComparator> a4 ( words . begin (), words . end () );
  assert ( a4 . min ( a4 . begin (), a4 . end () ) == "if" );

  CIntervalMin <string, CStrComparator> a5 ( words . begin (), words . end (), CStrComparator ( false ) );
  assert ( a5 . min ( a5 . begin (), a5 . end () ) == "NULL" );
  
  CIntervalMin <string, function<bool(const string &, const string &)> > a6 ( [] ( const string & a, const string & b )
  {
    return a > b;
  } );
  for ( const auto & w : words )
    a6 . push_back ( w );
  assert ( a6 . min ( a6 . begin (), a6 . end () ) == "void" );

     return 0;
}

#endif /* __PROGTEST__ */

