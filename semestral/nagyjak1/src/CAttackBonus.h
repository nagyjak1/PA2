#pragma once

#include "CBonus.h"

/**
 * When picked up boosts tank's damage for certain time.
 * @brief Class represents damage bonus.
 */
class CAttackBonus : public CBonus {
public:
    /**
     * Construct bonus at given position, which upgrades tank's attack damage.
     * @brief Basic constructor.
     * @param x X-axis position
     * @param y Y-axis position
     */
    CAttackBonus(int x, int y);

    /**
     * @brief Upgrades tank's attack damage.
     * @param tank shared_ptr<CTank> Tank which will be boosted
     * @param boosts vector<shared_ptr<CBonus>> Vector to keep track of active bonuses
     */
    void Boost(shared_ptr<CTank> tank, vector<shared_ptr<CBonus>> &boosts) override;

    /**
     * Change the tank's attack damage to the state before the bonus was picked.
     * @brief End the effect of the boost.
     */
    void UnBoost() const override;
};


