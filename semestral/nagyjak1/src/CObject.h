#pragma once

#include <ncurses.h>
#include <map>
#include <memory>
#include "CONSTANTS.h"

using namespace std;

/**
 * Object has it's position in map and it's display character, can be printed on the screen and can take damage.
 * @brief Class represents basic game object.
 */
class CObject {
public:
    /**
     * Constructs object at the given position.
     * @brief Basic constructor.
     * @param x X-axis position.
     * @param y Y-axis position.
     */
    CObject(int x, int y)
            : m_X(x),
              m_Y(y),
              m_DisplayChar(' ') {
    }

    /**
     * @brief Get the X axis position of the object.
     * @return The X axis value.
     */
    int GetX() const { return m_X; }

    /**
     * @brief Get the Y axis position of the object.
     * @return The Y axis value.
     */
    int GetY() const { return m_Y; }

    /**
     * @brief Print object's display character at object's position.
     */
    void Show() const { mvaddch (m_Y, m_X, m_DisplayChar); }

    /**
     * Print space character at the object's position.
     * @brief Hides object on the screen.
     */
    void Hide() const { mvaddch (m_Y, m_X, ' '); }

    /**
     * @brief Lower object's HP by given number.
     * @param damage Damage dealt to the object.
     * @return true If object is destroyed.
     * @return false If object is not destroyed.
     */
    virtual bool TakeDamage(int damage) {
        return false;
    }

    virtual void ResetHP() {}

protected:
    /**
     * X-axis position
     */
    int m_X;

    /**
     * Y-axis position
     */
    int m_Y;

    /**
     * Character representing the object when printed on the screen
     */
    char m_DisplayChar;
};


