#include "CPlayMode.h"

enum GAME_STATUS {
    LOSE, WIN, NONE, END, PAUSE, RESIZE, SMALL_SCREEN
};

CPlayMode::CPlayMode()
        : m_Level(1) {
}

int CPlayMode::HandleEvents() {
    string mapFile = CMap::SelectMap(MAP_FOLDER);

    if (mapFile.empty()) {
        NoMapFile();
        return 3;
    }
    m_Map.LoadMap(mapFile);
    clear();
    m_Map.Display();
    ShowStats();

    int gameStatus;
    while (true) {
        gameStatus = GameUpdate();
        SmallScreenCheck();
        switch (gameStatus) {
            case LOSE:
                Lose();
                return 3;
            case WIN:
                Win();
                break;
            case END:
                return 3;
            case PAUSE:
                Pause();
                clear();
                m_Map.Display();
                ShowStats();
                break;
            case RESIZE:
                clear();
                m_Map.Display();
                ShowStats();
                break;
            default:
                break;
        }
    }
}

int CPlayMode::GameUpdate() {
    m_Map.Update();
    // GetMove() returns 1 if p/P is pressed, 2 if BACKSPACE is pressed and 3 if screen is resized
    auto x = m_Map.m_Player->GetMove(m_Map.m_Bullets);
    switch (x) {
        case 1:
            return PAUSE;
        case 2:
            return END;
        case 3:
            return RESIZE;
        default:
            break;
    }
    m_Map.m_Enemy->GetMove(m_Map.m_Bullets, m_Map.m_Player);
    ShowStats();

    if (m_Map.m_Player->Destroyed())
        return LOSE;
    if (m_Map.m_Enemy->Destroyed())
        return WIN;
    return NONE;
}


void CPlayMode::DisplayWin() const {
    int xMax = getmaxx (stdscr);
    clear();
    box(stdscr, 0, 0);
    string message;
    message = "THE ENEMY TANK WAS DESTROYED";
    mvprintw(3, int(xMax / 2 - message.length() / 2), message.c_str());
    message = "LEVEL " + to_string(m_Level) + " COMPLETED";
    mvprintw(5, int(xMax / 2 - message.length() / 2), message.c_str());
    message = "< PRESS ENTER TO CONTINUE >";
    mvprintw(6, int(xMax / 2 - message.length() / 2), message.c_str());
}

void CPlayMode::DisplayLose() {
    int xMax = getmaxx (stdscr);
    clear();
    box(stdscr, 0, 0);
    string message;
    message = "YOUR TANK WAS DESTROYED";
    mvprintw(3, int(xMax / 2 - message.length() / 2), message.c_str());
    message = "YOU LOST";
    mvprintw(5, int(xMax / 2 - message.length() / 2), message.c_str());
    message = "< PRESS ENTER TO CONTINUE >";
    mvprintw(6, int(xMax / 2 - message.length() / 2), message.c_str());
}

void CPlayMode::DisplayName() const {
    int xMax = getmaxx (stdscr);
    clear();
    box(stdscr, 0, 0);
    string message;
    message = "YOUR REACHED LEVEL " + to_string(m_Level);
    mvprintw(2, int(xMax / 2 - message.length() / 2), message.c_str());
    message = "YOUR SCORE IS " + to_string(m_Level - 1);
    mvprintw(3, int(xMax / 2 - message.length() / 2), message.c_str());
    message = "ENTER YOUR NAME";
    mvprintw(5, int(xMax / 2 - message.length() / 2), message.c_str());
    message = "< PRESS SPACE TO CONTINUE >";
    mvprintw(8, int(xMax / 2 - message.length() / 2), message.c_str());
}

void CPlayMode::Lose() const {
    DisplayLose();
    int choice = getch();
    while (choice != '\n') {
        choice = getch();
        if (choice == KEY_RESIZE)
            DisplayLose();
    }
    EnterName();
}

void CPlayMode::EnterName() const {
    int xMax = getmaxx (stdscr);
    DisplayName();
    int choice = getch();
    string name;
    while (choice != ' ') {
        choice = getch();
        if (choice == KEY_RESIZE) {
            DisplayName();
            xMax = getmaxx (stdscr);
            mvprintw(6, int(xMax / 2 - name.length() / 2), name.c_str());
        }
        if (choice == KEY_BACKSPACE) {
            if (name.length() > 0) {
                name.pop_back();
                mvprintw(6, int(xMax / 2 - name.length()), "                       ");
                mvprintw(6, int(xMax / 2 - name.length() / 2), name.c_str());
                continue;
            } else
                continue;
        }

        if (name.length() == 20 || !(('0' <= choice && choice <= '9') ||
                                     ('a' <= choice && choice <= 'z') ||
                                     ('A' <= choice && choice <= 'Z')))
            continue;

        else {
            name.push_back(char(choice));
            mvprintw(6, int(xMax / 2 - name.length() / 2), name.c_str());
        }
    }
    if (name.empty())
        name = "Player";
    SavePlayer(name, m_Level - 1);
}

void CPlayMode::Win() {
    DisplayWin();
    int choice = getch();
    while (choice != '\n') {
        choice = getch();
        if (choice == KEY_RESIZE)
            DisplayWin();
    }

    m_Map.Reset();
    m_Map.m_Enemy->MakeStronger(m_Level);
    m_Map.m_Player->MakeStronger(m_Level - 1);
    // player is upgraded every 2 levels
    if (m_Level % 2 == 0)
        m_Map.m_Player->MakeStronger(m_Level);
    m_Level++;

    clear();
    m_Map.Display();
}

void CPlayMode::ShowStats() const {
    if (m_Map.m_Player == nullptr || m_Map.m_Enemy == nullptr)
        return;
    string message;
    message = "LEVEL " + to_string(m_Level);
    mvprintw(m_Map.GetSizeY() + 1, int(m_Map.GetSizeX() / 2 - message.length() / 2), message.c_str());

    message = "YOU:";
    mvprintw(m_Map.GetSizeY() + 2, 0, message.c_str());
    message = to_string(m_Map.m_Player->GetHealth()) + " HP     ";
    mvprintw(m_Map.GetSizeY() + 3, 0, message.c_str());
    message = to_string(m_Map.m_Player->GetArmor()) + " ARMOR    ";
    mvprintw(m_Map.GetSizeY() + 4, 0, message.c_str());

    message = "ENEMY:";
    mvprintw(m_Map.GetSizeY() + 2, int(m_Map.GetSizeX() - message.length()), message.c_str());
    message = "    " + to_string(m_Map.m_Enemy->GetHealth()) + " HP";
    mvprintw(m_Map.GetSizeY() + 3, int(m_Map.GetSizeX() - message.length()), message.c_str());
    message = "    " + to_string(m_Map.m_Enemy->GetArmor()) + " ARMOR";
    mvprintw(m_Map.GetSizeY() + 4, int(m_Map.GetSizeX() - message.length()), message.c_str());
}

void CPlayMode::SavePlayer(const string &name, const int &score) {
    pair<string, int> newScore(name, score);
    CLeaderboardMode leaderboard;
    leaderboard.LoadLeaderBoard();
    auto &x = leaderboard.GetTopTen();

    // check if given score fits into previous scores
    for (auto it = x.begin(); it < x.end(); ++it)
        if ((*it).second <= score) {
            x.insert(it, newScore);
            x.pop_back();
            break;
        }
    // write changed top 10 players back to the file
    ofstream fileOut("./examples/Leaderboard.txt");
    if (fileOut.is_open()) {
        for (int i = 0; i < 10; ++i)
            fileOut << x.at(i).second << ',' << x.at(i).first << endl;
        fileOut.close();
    }
}

void CPlayMode::Pause() {
    DisplayPause();
    int choice = getch();
    while (choice != ' ') {
        choice = getch();
        if (choice == KEY_RESIZE)
            DisplayPause();
    }
}

void CPlayMode::DisplayPause() {
    int xMax = getmaxx (stdscr);
    clear();
    box(stdscr, 0, 0);
    string message = "GAME PAUSED";
    mvprintw(2, int(xMax / 2 - message.length() / 2), message.c_str());
    message = "< PRESS SPACE TO CONTINUE >";
    mvprintw(4, int(xMax / 2 - message.length() / 2), message.c_str());
}

void CPlayMode::NoMapFile() {
    DisplayNoMapFile();
    int choice = getch();
    while (choice != ' ') {
        choice = getch();
        if (choice == KEY_RESIZE)
            DisplayNoMapFile();
    }
}

void CPlayMode::DisplayNoMapFile() {
    int xMax = getmaxx (stdscr);
    clear();
    box(stdscr, 0, 0);
    string message = "NO MAP FILE FOUND";
    mvprintw(2, int(xMax / 2 - message.length() / 2), message.c_str());
    message = "< PRESS SPACE TO CONTINUE >";
    mvprintw(4, int(xMax / 2 - message.length() / 2), message.c_str());
}

void CPlayMode::DisplaySmallScreen() {
    clear();
    int xMax = getmaxx(stdscr);
    string message = "WINDOW TOO SMALL";
    mvprintw(2, int(xMax / 2 - message.length() / 2), message.c_str());
    message = "PLEASE RESIZE";
    mvprintw(4, int(xMax / 2 - message.length() / 2), message.c_str());
}

void CPlayMode::SmallScreenCheck() const {
    int xMax, yMax;
    getmaxyx (stdscr, yMax, xMax);
    if (!(xMax < m_Map.GetSizeX() + 1 || yMax < m_Map.GetSizeY() + 5))
        return;
    DisplaySmallScreen();
    int choice = getch();
    while (xMax < m_Map.GetSizeX() + 1 || yMax < m_Map.GetSizeY() + 5) {
        getmaxyx (stdscr, yMax, xMax);
        choice = getch();
        if (choice == KEY_RESIZE)
            DisplaySmallScreen();
    }
    clear();
}

