#pragma once

#include "CObject.h"
#include "CRock.h"
#include "CWall.h"
#include "CPlayer.h"
#include "CEnemy.h"
#include "CBullet.h"
#include <vector>
#include <map>
#include <fstream>
#include <string>
#include <memory>
#include "CONSTANTS.h"
#include <filesystem>
#include <set>

using namespace std;

/**
 * Map is loaded from file (walls, rocks and tanks are loaded). When the game is played, it keeps track of all other game
 * objects (bullets, bonuses and applied bonuses = boosts).
 * @brief Class representing map in game.
 */
class CMap {
public:
    /**
     * @brief Print all map's objects, player and enemy.
     */
    void Display() const;

    /**
     * Get map's width.
     * @return int Map width.
     */
    int GetSizeX() const { return m_Width; }

    /**
     * Get map's height.
     * @return int Map height.
     */
    int GetSizeY() const { return m_Height; }

    /**
     * @brief Reset map to the state when it was loaded from file.
     */
    void Reset();

    /**
     * @brief Find all valid map files in given directory and let user choose one map file to play with.
     * @param directory Directory to search for map files
     * @return string Path to selected map file.
     */
    static string SelectMap(const string &directory);

    /**
     * Map file must contain only allowed characters ('W', 'R', ' ', 'P', 'E'), must be surrounded by walls and must have
     * just one 'P' and one 'E' character.
     * @brief Check if file at given path is valid map file.
     * @param file Path to file.
     * @return true If file is valid map file.
     * @return false If file is not valid map file.
     */
    static bool MapIsValid(const string &file);

    /**
     * @brief Check if walls create rectangle/square around the map.
     * @param walls set<pair<int,int>> & walls Set of walls coordinates
     * @param xSize Max X-axis value in map
     * @param ySize Max Y-axis value in map
     * @return true If map is surrounded by walls.
     * @return false Otherwise.
     */
    static bool MapWallsCheck(const set<pair<int, int>> &walls, int xSize, int ySize);

    /**
     * Load objects, player and enemy into variables.
     * @brief Load map file at given path.
     * @param path Path to file
     */
    void LoadMap(const string &path);

    /**
     * Bonus is invalid if it expired or if it was picked by tank.
     * @brief Iterate through all bonuses and check if they are valid or if they are picked.
     */
    void UpdateBonuses();

    /**
     * Boost is invalid if it expired.
     * @brief Iterate through all boosts and check if they are valid.
     */
    void UpdateBoosts();

    /**
     * @brief Iterate through all bullets and move them and check for collisions.
     */
    void UpdateBullets();

    /**
     * @brief Update all map objects.
     */
    void Update();

    map<pair<int, int>, shared_ptr<CObject>> m_Objects;
    map<pair<int, int>, shared_ptr<CBonus>> m_Bonuses;
    shared_ptr<CPlayer> m_Player;
    shared_ptr<CEnemy> m_Enemy;
    vector<shared_ptr<CBullet>> m_Bullets;
    vector<shared_ptr<CBonus>> m_Boosts;
private:

    /**
     * Map size - width.
     */
    int m_Width;

    /**
     * Map size - height.
     */
    int m_Height;

    /**
     * Map of objects when loaded from file.
     */
    map<pair<int, int>, shared_ptr<CObject>> m_Original_Objects;
};


