#include "CBulletSpeedBonus.h"

CBulletSpeedBonus::CBulletSpeedBonus(int x, int y)
        : CBonus(x, y) {
    m_DisplayChar = BULLET_SPEED_BONUS;
}

void CBulletSpeedBonus::Boost(shared_ptr<CTank> tank, vector<shared_ptr<CBonus>> &boosts) {
    // tank can be boosted by only one bonus, remove previous boost before boosting with new one
    if (tank->Boosted())
        UnboostTank(tank, boosts);
    m_Picked = true;
    m_BoostedTank = tank;
    m_BoostTime = chrono::steady_clock::now();
    m_OriginalValue = tank->GetBulletSpeed();
    tank->GetBulletSpeed() /= BULLET_SPEED_MULTIPLIER;
    boosts.push_back(make_shared<CBulletSpeedBonus>(*this));
    tank->Boost();
}

void CBulletSpeedBonus::UnBoost() const {
    m_BoostedTank->GetBulletSpeed() = m_OriginalValue;
    m_BoostedTank->Boost();
}