#pragma once

#include "CObject.h"
#include "CBullet.h"
#include <memory>
#include <vector>
#include <map>
#include <cmath>
#include "CONSTANTS.h"

using namespace std;
enum directions {
    UP, DOWN, LEFT, RIGHT
};

/**
 * Class represents tank. Tank can fire bullets, tank can take damage, tank can move down, up, right or left. Tank can get boosted
 * when it picks up bonus on the map.
 * @brief Class which represent tank, the main object in the game.
 */
class CTank : public CObject {
public:
    /**
     * @brief Basic constructor.
     * Constructs new tank with given parameters.
     * @param x X-axis position
     * @param y Y-axis position
     * @param speed Tank's movement speed
     * @param bulletSpeed Tank's bullet speed
     * @param damage Tank's damage
     * @param attackSpeed Tank's attack speed
     * @param health Tank's health
     * @param armor Tank's armor
     */
    CTank(int x, int y, int speed, int bulletSpeed, int damage, int attackSpeed, int health, int armor);

    /**
     * @brief Move the tank up by one if move is possible.
     */
    void MoveUp();

    /**
     * @brief Move the tank down by one if move is possible.
     */
    void MoveDown();

    /**
     * @brief Move the tank left by one if move is possible.
     */
    void MoveLeft();

    /**
     * @brief Move the tank right by one if move is possible.
     */
    void MoveRight();

    /**
     * Check if tank can move to given position. Tank can't move if time from last move is less than m_Speed or if the
     * position is occupied by another object (Wall, Rock or Enemy).
     * @brief Decide if tank can move to the given position.
     * @param x X-axis position
     * @param y Y-axis position
     * @return true If tank can move to the given position.
     * @return false If tank can't move to the given position.
     */
    bool CanMove(int x, int y) const;

    /**
     * Deal damage to the tank. First use tank's armor, if armor is zero, use tank's health.
     * @brief Deal given damage to the tank.
     * @param damage Damage dealt to the tank
     * @return true If tank is destroyed.
     * @return false If tank is not destroyed.
     */
    bool TakeDamage(int damage) override;

    /**
     * @brief Decide if tank is destroyed.
     * @return true If tank's HP is less or equal to zero.
     * @return false If tank's HP is more than zero.
     */
    bool Destroyed() const { return (m_Health <= 0); }

    /**
     * @brief Upgrade tank's stats according to level of game.
     * @param level Level of the game to count with.
     */
    void MakeStronger(int level);

    /**
     * @brief Get tank's original max HP.
     * @return int Tank's max health.
     */
    int GetMaxHealth() const { return m_MaxHealth; }

    /**
     * @brief Get reference to tank's speed.
     * @return int & Tank's speed reference.
     */
    int &GetSpeed() { return m_Speed; }

    /**
     * @brief Get reference to tank's damage.
     * @return int & Tank's damage reference.
     */
    int &GetDamage() { return m_Damage; }

    /**
     * @brief Get reference to tank's bullet speed.
     * @return int & Tank's bullet speed reference.
     */
    int &GetBulletSpeed() { return m_BulletSpeed; }

    /**
     * @brief Get reference to tank's health.
     * @return int & Tank's health.
     */
    int &GetHealth() { return m_Health; }

    /**
     * @brief Get reference to tank's armor.
     * @return int & Tank's armor reference.
     */
    int &GetArmor() { return m_Armor; }

    /**
     * @brief Get reference to tank's attack speed.
     * @return int & Tank's attack speed reference.
     */
    int &GetAttackSpeed() { return m_AttackSpeed; }

    /**
     * @brief Get info if tank is boosted.
     * @return true If tank is boosted.
     * @return false If tank is not boosted.
     */
    bool Boosted() const { return m_Boosted; }

    /**
     * @brief Negate m_Boosted variable.
     */
    void Boost() { m_Boosted = !m_Boosted; }

protected:
    /**
     * Last movement direction.
     */
    int m_Direction;

    /**
     * Time in ms between moves.
     */
    int m_Speed;

    /**
     * Time in ms between bullet moves.
     */
    int m_BulletSpeed;

    /**
     * Tank's bullet's damage.
     */
    int m_Damage;

    /**
     * Time in ms between bullets fired.
     */
    int m_AttackSpeed;

    /**
     * Tank's health.
     */
    int m_Health;

    /**
     * Tank's armor.
     */
    int m_Armor;

    /**
     * Tank's max health.
     */
    int m_MaxHealth;

    /**
     * Time of last move.
     */
    chrono::time_point<chrono::steady_clock> m_LastMoved;

    /**
     * Time of last bullet fire.
     */
    chrono::time_point<chrono::steady_clock> m_LastFired;

    /**
     * Variable if tank picked a bonus and is boosted by the bonus.
     */
    bool m_Boosted;

    /**
     * The tank's spawn position when map is loaded from file
     */
    pair<int, int> m_SpawnPosition;
};


