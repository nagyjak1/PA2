#pragma once

#include <string>
#include "ncurses.h"

using namespace std;

/**
 * @brief Welcome menu class shown when the program is opened.
 */
class CWelcomeMenu {
public:
    /**
     * @brief Display a welcome message on the screen.
     */
    static void Display() ;

    /**
     * Wait until the user presses SPACE.
     * @brief Handle input from the user.
     * @return 1 If the SPACE key is pressed.
     */
    static int HandleEvents() ;
};


