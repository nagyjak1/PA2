#pragma once

#include "CBonus.h"
#include <climits>

/**
 * When picked up heals the tank for percentage of tank's HP.
 * @brief Class which represents heal bonus.
 */
class CHealthBonus : public CBonus {
public:
    /**
     * Construct bonus at given position, which heals the tank.
     * @brief Basic constructor.
     * @param x X-axis position
     * @param y Y-axis position
     */
    CHealthBonus(int x, int y);

    /**
     * Heals the tank for percentage of it's HP. If max HP is reached, tank's armor is increased.
     * @brief Heals the tank.
     * @param tank shared_ptr<CTank> Tank which will be healed.
     * @param boosts vector<shared_ptr<CBonus>> Vector to keep track of active bonuses
     */
    void Boost(shared_ptr<CTank> tank, vector<shared_ptr<CBonus>> &boosts) override;

    /**
     * @brief Heal effect is permanent, unboost doesn't do anything.
     */
    void UnBoost() const override;
};


