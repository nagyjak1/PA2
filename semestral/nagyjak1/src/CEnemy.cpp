#include "CEnemy.h"

CEnemy::CEnemy(int x, int y, int speed, int bulletSpeed, int damage, int attackSpeed, int health, int armor)
        : CTank(x, y, speed, bulletSpeed, damage, attackSpeed, health, armor) {
    m_LastDecision = chrono::steady_clock::now();
}

void CEnemy::GetMove(vector<shared_ptr<CBullet>> &bullets, shared_ptr<CTank> const & enemy) {

    // enemy is in same horizontal line, turn to enemy and fire
    if (enemy->GetY() == m_Y) {
        if (enemy->GetX() < m_X) {
            if (m_Direction != LEFT)
                MoveLeft();
            Fire(bullets);
            return;
        }
        if (enemy->GetX() > m_X) {
            if (m_Direction != RIGHT)
                MoveRight();
            Fire(bullets);
            return;
        }
    }
    // enemy is in same vertical line, turn to enemy and fire
    if (enemy->GetX() == m_X) {
        if (enemy->GetY() < m_Y) {
            if (m_Direction != UP)
                MoveUp();
            Fire(bullets);
            return;
        }
        if (enemy->GetY() > m_Y) {
            if (m_Direction != DOWN)
                MoveDown();
            Fire(bullets);
            return;
        }
    }
    // decision timer to make the PC less intelligent
    if (chrono::duration_cast<chrono::milliseconds>(chrono::steady_clock::now() - m_LastDecision).count() <= ENEMY_DECISION_TIME)
        return;
    m_LastDecision = chrono::steady_clock::now();

    // random movement but move closer to player, if rock is in front, fire into rock
    auto x = random() % 4;
    switch (x) {
        case 0:
            if (enemy->GetY() > m_Y) {
                MoveDown();
                if (mvinch(m_Y + 1, m_X) == ROCK)
                    Fire(bullets);
            }
            break;
        case 1:
            if (enemy->GetY() < m_Y) {
                MoveUp();
                if (mvinch(m_Y - 1, m_X) == ROCK)
                    Fire(bullets);
            }
            break;
        case 2:
            if (enemy->GetX() > m_X) {
                MoveRight();
                if (mvinch(m_Y, m_X + 1) == ROCK)
                    Fire(bullets);
            }
            break;
        case 3:
            if (enemy->GetX() < m_X) {
                MoveLeft();
                if (mvinch(m_Y, m_X - 1) == ROCK)
                    Fire(bullets);
            }
            break;
    }
    // look around the tank to see bonuses, if bonus is around tank, pick it up
    if (isBonus(m_Y - 1, m_X))
        MoveUp();
    if (isBonus(m_Y + 1, m_X))
        MoveDown();
    if (isBonus(m_Y, m_X + 1))
        MoveRight();
    if (isBonus(m_Y, m_X - 1))
        MoveLeft();
}

bool CEnemy::isBonus(int y, int x) {
    return (mvinch(y, x) == DAMAGE_BONUS || mvinch(y, x) == SPEED_BONUS || mvinch(y, x) == HP_BONUS ||
            mvinch(y, x) == BULLET_SPEED_BONUS || mvinch(y, x) == ATTACK_SPEED_BONUS);
}


void CEnemy::Fire(vector<shared_ptr<CBullet>> &bullets) {
    // if time between firing is less than attackSpeed value, tank can't fire
    if (chrono::duration_cast<chrono::milliseconds>(chrono::steady_clock::now() - m_LastFired).count() <= m_AttackSpeed)
        return;
    m_LastFired = chrono::steady_clock::now();

    // bullet is created in front of the tank based on it's direction
    int x, y;
    switch (m_Direction) {
        case UP:
            x = m_X;
            y = m_Y - 1;
            break;
        case DOWN:
            x = m_X;
            y = m_Y + 1;
            break;
        case LEFT:
            x = m_X - 1;
            y = m_Y;
            break;
        case RIGHT:
            x = m_X + 1;
            y = m_Y;
            break;
    }
    bullets.emplace_back(make_shared<CBullet>(x, y, m_BulletSpeed, m_Damage, m_Direction, false));
}
