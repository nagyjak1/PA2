#pragma once

#include "CObject.h"
#include "CTank.h"
#include <memory>
#include "CONSTANTS.h"

using namespace std;

/**
 * Bonuses may be dropped by rocks when rocks are destroyed. There are different types of bonuses (damage, speed, bullet speed,
 * attack speed and heal). When tank gets to bonus's position, it picks it up and gets boosted for a specific time. If
 * already boosted tank picks up another bonus, the previous boost is deactivated and tank gets new boost. Bonus disappears if
 * no one picks it up in specific time.
 * @brief Abstract class represents types of bonuses, which can be picked up by a tank and which improve tank's stats.
 */
class CBonus : public CObject {
public:
    CBonus(int x, int y)
            : CObject(x, y),
              m_Duration(BONUS_DURATION),
              m_BoostDuration(BONUS_EFFECT_DURATION),
              m_SpawnTime(chrono::steady_clock::now()),
              m_Picked(false),
              m_OriginalValue(0) {
    }

    bool Picked() const { return m_Picked; }

    /**
     * @brief Check if bonus expired.
     * @return true If bonus expired.
     * @return false Otherwise.
     */
    bool TimeOut() const {
        return chrono::duration_cast<chrono::milliseconds>(chrono::steady_clock::now() - m_SpawnTime).count() >
               m_Duration;
    }

    virtual void Boost(shared_ptr<CTank> tank, vector<shared_ptr<CBonus>> &boosts) = 0;

    virtual void UnBoost() const = 0;

    /**
     * @brief Check if bonus's boost expired.
     * @return true If boost duration expired.
     * @return false Otherwise.
     */
    bool NotValid() const {
        return chrono::duration_cast<chrono::milliseconds>(chrono::steady_clock::now() - m_BoostTime).count() >
               m_BoostDuration;
    }

    /**
     * @brief Get tank which picked up the bonus.
     * @return shared_ptr<CTank> Shared pointer to tank which picked the bonus.
     */
    shared_ptr<CTank> BoostedTank() const { return m_BoostedTank; }

    /**
     * @brief Remove boost from given tank.
     * @param tank Tank to remove boost from.
     * @param boosts Vector of active boosts.
     */
    static void UnboostTank(shared_ptr<CTank> const &tank, vector<shared_ptr<CBonus>> &boosts) {
        for (auto it = boosts.begin(); it != boosts.end(); ++it)
            if ((*it)->BoostedTank() == tank) {
                (*it)->UnBoost();
                boosts.erase(it);
                break;
        }
    }

protected:
    /**
     * Time in ms to despawn the bonus.
     */
    int m_Duration;

    /**
     * Effect's duration in ms.
     */
    int m_BoostDuration;

    /*
     * Time when the bonus spawned.
     */
    chrono::time_point<chrono::steady_clock> m_SpawnTime;

    /**
     * Bonus was already picked.
     */
    bool m_Picked;

    /*
     * Time when the bonus was picked.
     */
    chrono::time_point<chrono::steady_clock> m_BoostTime;

    /*
     * Value before the tank was boosted by the bonus.
     */
    int m_OriginalValue;

    /*
     * Shared pointer to the tank that picked the bonus.
     */
    shared_ptr<CTank> m_BoostedTank;
};


