#include "CSpeedBonus.h"

CSpeedBonus::CSpeedBonus(int x, int y)
        : CBonus(x, y) {
    m_DisplayChar = SPEED_BONUS;
}

void CSpeedBonus::Boost(shared_ptr<CTank> tank, vector<shared_ptr<CBonus>> &boosts) {
    // tank can be boosted by only one bonus, remove previous boost before boosting with new one
    if (tank->Boosted())
        UnboostTank(tank, boosts);
    m_Picked = true;
    m_BoostedTank = tank;
    m_BoostTime = chrono::steady_clock::now();
    m_OriginalValue = tank->GetSpeed();
    tank->GetSpeed() /= SPEED_MULTIPLIER;
    boosts.push_back(make_shared<CSpeedBonus>(*this));
    tank->Boost();
}

void CSpeedBonus::UnBoost() const {
    m_BoostedTank->GetSpeed() = m_OriginalValue;
    m_BoostedTank->Boost();
}