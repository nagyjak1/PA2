#include "CLeaderboardMode.h"

void CLeaderboardMode::Display() {
    clear();
    if (LoadLeaderBoard()) {
        int xMax, yMax, x, y = 2;
        getmaxyx (stdscr, yMax, xMax);

        box(stdscr, 0, 0);
        string message = "LEADERBOARD";
        mvprintw(y, xMax / 2 - int(message.length() / 2), message.c_str());
        getyx (stdscr, y, x);
        y += 2;
        for (int i = 0; i < 10; ++i) {
            if (y + i > yMax - 1)
                break;
            mvprintw(y + i, xMax / 2 - 12, m_TopTen[i].first.c_str());
            mvprintw(y + i, xMax / 2 + 12, to_string(m_TopTen[i].second).c_str());

        }
        getyx (stdscr, y, x);
        message = "< PRESS SPACE TO CONTINUE >";
        mvprintw(y + 2, xMax / 2 - int(message.length() / 2), message.c_str());
    } else

        // file Leaderboard.txt wasn't loaded successfully
        DisplayNoLeaderboardFile();
}

int CLeaderboardMode::HandleEvents() {
    Display();
    int choice;
    while (true) {
        choice = getch();
        switch (choice) {
            case KEY_RESIZE:
                Display();
                break;
            case ' ':
                return 3;
            default:
                break;
        }
    }
}

bool CLeaderboardMode::LoadLeaderBoard() {
    m_TopTen.clear();

    for (auto i = 0; i < 10; i++)
        m_TopTen.emplace_back("---", 0);

    char c;
    ifstream file("./examples/Leaderboard.txt");
    if (file.is_open()) {
        for (auto i = 0; i < 10; i++) {
            if (file.eof())
                break;
            if ( ! ( file >> m_TopTen[i].second >> c >> m_TopTen[i].first) || c != ',')
                // file is damaged, has wrong format
                return false;
        }
        file.close();
        return true;
    } else
        // file cant be opened
        return false;
}

void CLeaderboardMode::DisplayNoLeaderboardFile() {
    int xMax = getmaxx (stdscr);
    clear();
    box(stdscr, 0, 0);
    string message = "FILE 'examples/Leaderboard.txt' NOT FOUND OR DAMAGED";
    mvprintw(2, int(xMax / 2 - message.length() / 2), message.c_str());
    message = "CAN'T LOAD THE SCORE";
    mvprintw(4, int(xMax / 2 - message.length() / 2), message.c_str());
    message = "< PRESS SPACE TO CONTINUE >";
    mvprintw(6, int(xMax / 2 - message.length() / 2), message.c_str());
}