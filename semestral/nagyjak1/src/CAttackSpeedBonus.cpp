#include "CAttackSpeedBonus.h"

CAttackSpeedBonus::CAttackSpeedBonus(int x, int y)
        : CBonus(x, y) {
    m_DisplayChar = ATTACK_SPEED_BONUS;
}

void CAttackSpeedBonus::Boost(shared_ptr<CTank> tank, vector<shared_ptr<CBonus>> &boosts) {
    // tank can be boosted by only one bonus, remove previous boost before boosting with new one
    if (tank->Boosted())
        UnboostTank(tank, boosts);
    m_Picked = true;
    m_BoostedTank = tank;
    m_BoostTime = chrono::steady_clock::now();
    m_OriginalValue = tank->GetAttackSpeed();
    tank->GetAttackSpeed() /= ATTACK_SPEED_MULTIPLIER;
    boosts.push_back(make_shared<CAttackSpeedBonus>(*this));
    tank->Boost();
}

void CAttackSpeedBonus::UnBoost() const {
    m_BoostedTank->GetAttackSpeed() = m_OriginalValue;
    m_BoostedTank->Boost();
}