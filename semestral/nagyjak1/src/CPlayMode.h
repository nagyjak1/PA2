#pragma once

#include "CMap.h"
#include "CLeaderboardMode.h"

/**
 * Keeps track of current map and level and handles different events during playing.
 * @brief Main class for playing the game.
 */
class CPlayMode {
public:
    /**
     * Constructs CPlayMode object and sets its level to 1.
     * @brief Basic constructor.
     */
    CPlayMode();

    /**
     * Load maps from folder, let user select one, load the selected map. Handles different game states (WINNING, LOSING,
     * PAUSING, RESIZING, EXITING).
     * @brief Inits game and handles different game events.
     * @return 3 If playmode is exited.
     */
    int HandleEvents();

    /**
     * @brief Display losing screen and waits for user to enter his name to save his score.
     */
    void Lose() const;

    /**
     * @brief Display winning screen, waits for user to press ENTER and moves the game to next level.
     */
    void Win();

    /**
     * @brief Handle user input to enter his name and save given name and score into Leaderboard.txt.
     */
    void EnterName() const;

    /**
     * @brief Save give name and score to Leaderboard.txt if score fits to the board.
     * @param name Player's name
     * @param score Player's score - last defeated level
     */
    static void SavePlayer(const string &name, const int &score);

    /**
     * @brief Display pause screen, pauses game and waits for the user to press SPACE.
     */
    static void Pause();

    /**
     * @brief Display no map file screen and waits for the user to press SPACE.
     */
    static void NoMapFile();

    /**
     * Update all map objects, get move from player, get move from enemy and check if player won or lost.
     * @brief Get input from the user and update game status.
     * @return PAUSE If user presses P or p.
     * @return END If user presses BACKSPACE.
     * @return RESIZE If user resizes screen.
     * @return LOSE If player's tank is destroyed.
     * @return WIN If enemy's tank is destroyed.
     * @return NONE Otherwise.
     */
    int GameUpdate();

private:
    /**
     * @brief Display losing screen.
     */
    static void DisplayLose();

    /**
     * @brief Display winning screen.
     */
    void DisplayWin() const;

    /**
     * @brief Display enter name request screen.
     */
    void DisplayName() const;

    /**
     * @brief Display pause screen.
     */
    static void DisplayPause();

    /**
     * @brief Display current level, player's and enemy's HP and ARMOR under the map on the screen.
     */
    void ShowStats() const;

    /**
     * @brief Display no map file found screen.
     */
    static void DisplayNoMapFile();

    /**
    * @brief Waits until user resizes screen to correct size.
    */
    void SmallScreenCheck() const;

    /**
     * @brief Displays screen too small error.
     */
    static void DisplaySmallScreen();


    /**
     * Selected game map.
     */
    CMap m_Map;

    /**
     * Current game level.
     */
    int m_Level;
};


