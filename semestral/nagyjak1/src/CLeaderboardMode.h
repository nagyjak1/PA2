#pragma once

#include <string>
#include <vector>
#include "ncurses.h"
#include <fstream>

using namespace std;

/**
 * @brief Class to shows the top 10 players loaded from file and to handle input from the user.
 */
class CLeaderboardMode {
public:
    /**
     * @brief Displays the top 10 players loaded from file examples/Leaderboard.txt.
     */
    void Display();

    /**
     * Waits until user presses SPACE, handles RESIZE of the screen.
     * @brief Handles input from the user.
     * @return 3 if the SPACE key is pressed
     */
    int HandleEvents();

    /**
     * @brief Loads top 10 players from file examples/Leaderboard.txt.
     * @return
     */
    bool LoadLeaderBoard();

    /**
     * @brief Displays Leaderboard.txt file not found message.
     */
    static void DisplayNoLeaderboardFile();

    vector<pair<string, int>> &GetTopTen() { return m_TopTen; }

private:
    /**
     * Top 10 players - pair of name and score for each
     */
    vector<pair<string, int>> m_TopTen;
};


