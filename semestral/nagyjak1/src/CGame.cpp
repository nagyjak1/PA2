#include "CGame.h"

void CGame::ScreenSetup() {
    initscr();
    noecho();
    cbreak();
    curs_set(0);
    keypad(stdscr, true);
    nodelay(stdscr, true);
}

void CGame::Run() {
    ScreenSetup();

    int choice;

    CWelcomeMenu::Display();
    // CWelcomeMenu::HandleEvents() returns 3 when SPACE is pressed, so it takes the user to main menu
    choice = CWelcomeMenu::HandleEvents();

    while (true) {
        switch (choice) {
            case 0: {
                CPlayMode playMode;
                // playMode.HandleEvents() returns 3 when playing is quited, so it takes the user to main menu
                choice = playMode.HandleEvents();
                break;
            }
            case 1: {
                CLeaderboardMode leaderboardMode;
                leaderboardMode.Display();
                // leaderboardMode.HandleEvents() returns 3 when leaderboard screen is exited, mainmenu is displayed then
                choice = leaderboardMode.HandleEvents();
                break;
            }
            // when option "Quit" is selected in mainmenu
            case 2:
                endwin();
                return;
            case 3: {
                CMainMenuMode mainMenu;
                mainMenu.Display();
                // mainMenu.HandleEvents() returns number based on selected option
                choice = mainMenu.HandleEvents();
                break;
            }
            default:
                break;
        }
    }
}