#pragma once

#include "CObject.h"

/**
 * @brief Class which represents wall - undestroyable map object.
 */
class CWall : public CObject {
public:
    /**
     * @brief Basic constructor.
     * Construct wall object at given position.
     * @param x X-axis position
     * @param y Y-axis position
     */
    CWall(int x, int y);
};


