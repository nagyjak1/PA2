#include "CBullet.h"

enum directions {
    UP, DOWN, LEFT, RIGHT
};

CBullet::CBullet(int x, int y, int speed, int damage, int direction, bool firedByPlayer)
        : CObject(x, y),
          m_Speed(speed),
          m_Damage(damage),
          m_Direction(direction),
          m_LastMoved(chrono::steady_clock::now()),
          m_FiredByPlayer(firedByPlayer) {
    m_DisplayChar = BULLET;
}

void CBullet::Move() {
    Show();
    // m_speed time not passed from last move
    if (chrono::duration_cast<chrono::milliseconds>(chrono::steady_clock::now() - m_LastMoved).count() <= m_Speed)
        return;
    Hide();
    // else bullet can move, change its position according to it's direction
    m_LastMoved = chrono::steady_clock::now();
    switch (m_Direction) {
        case (DOWN):
            m_Y++;
            break;
        case (UP):
            m_Y--;
            break;
        case (RIGHT):
            m_X++;
            break;
        case (LEFT):
            m_X--;
            break;
    }
    Show();
}