#include "CTank.h"

CTank::CTank(int x, int y, int speed, int bulletSpeed, int damage, int attackSpeed, int health, int armor)
        : CObject(x, y),
          m_Direction(UP),
          m_Speed(speed),
          m_BulletSpeed(bulletSpeed),
          m_Damage(damage),
          m_AttackSpeed(attackSpeed),
          m_Health(health),
          m_Armor(armor),
          m_MaxHealth(health),
          m_LastMoved(chrono::steady_clock::now()),
          m_LastFired(chrono::steady_clock::now()),
          m_Boosted(false),
          m_SpawnPosition(make_pair(x, y)) {
    m_DisplayChar = TANK;
}


void CTank::MoveDown() {
    m_Direction = DOWN;
    if (!CanMove(m_X, m_Y + 1))
        return;
    Hide();
    m_Y++;
    Show();
    m_LastMoved = chrono::steady_clock::now();
}

void CTank::MoveUp() {
    m_Direction = UP;
    if (!CanMove(m_X, m_Y - 1))
        return;
    Hide();
    m_Y--;
    Show();
    m_LastMoved = chrono::steady_clock::now();
}

void CTank::MoveLeft() {
    m_Direction = LEFT;
    if (!CanMove(m_X - 1, m_Y))
        return;
    Hide();
    m_X--;
    Show();
    m_LastMoved = chrono::steady_clock::now();
}

void CTank::MoveRight() {
    m_Direction = RIGHT;
    if (!CanMove(m_X + 1, m_Y))
        return;
    Hide();
    m_X++;
    Show();
    m_LastMoved = chrono::steady_clock::now();
}

bool CTank::TakeDamage(int damage) {
    // if armor is zero, damage is dealt directly to health
    if (m_Armor == 0)
        m_Health -= damage;
    // else deal damage to armor
    else {
        m_Armor -= damage;
        if (m_Armor < 0) {
            m_Health -= abs(m_Armor);
            m_Armor = 0;
        }
    }
    // if tank is destroyed after damage taken return true
    if (m_Health <= 0) {
        Hide();
        return true;
    }
    return false;
}

bool CTank::CanMove(int x, int y) const {
    // check what is located on given position, cant move to positions where walls, rocks and tanks are located
    return !(chrono::duration_cast<chrono::milliseconds>(chrono::steady_clock::now() - m_LastMoved).count() <= m_Speed
             || mvinch (y, x) == ROCK || mvinch (y, x) == WALL || mvinch (y, x) == TANK);
}

void CTank::MakeStronger(int level) {
    // stop upgrading everything after level 25
    if ( level > 25 )
        level = 25;
    m_Damage = DEFAULT_DAMAGE + level * DAMAGE_UPGRADE;
    m_Health = DEFAULT_HP + level * HP_UPGRADE;
    m_Armor = 0;
    m_MaxHealth = m_Health;
    m_X = m_SpawnPosition.first;
    m_Y = m_SpawnPosition.second;
    // stop upgrading attackSpeed, bulletSpeed and speed after level 12 - game would be unplayable
    if ( level >= 12 )
        level = 12;
    m_AttackSpeed = int(DEFAULT_ATTACK_SPEED * pow(ATTACK_SPEED_UPGRADE_MULTIPLIER, level));
    m_BulletSpeed = int(DEFAULT_BULLET_SPEED * pow(BULLET_SPEED_UPGRADE_MULTIPLIER, level));
    m_Speed = int(DEFAULT_SPEED * pow(SPEED_UPGRADE_MULTIPLIER, level));
}