#pragma once

#include "CObject.h"
#include <memory>
#include <chrono>

using namespace std;

/**
 * Bullets remain in move until they hit something. Bullet's speed, damage and direction is defined by tank which fired the bullet.
 * Bullets don't hurt the tank they were fired from.
 * @brief Class represent bullets fired by tanks.
 */
class CBullet : public CObject {
public:
    /**
     * @brief Basic constructor
     * @param x X-axis position.
     * @param y Y-axis position.
     * @param speed Bullet's movement speed.
     * @param damage Bullet's damage.
     * @param direction Bullet's movement direction.
     * @param firedByPlayer
     */
    CBullet(int x, int y, int speed, int damage, int direction, bool firedByPlayer);

    /**
     * Move the bullet by one if the move is possible. Bullet can't move if it moved recently.
     * @brief Move the bullet by one in bullet's direction if possible.
     */
    void Move();

    /**
     * @brief Get bullet's damage.
     * @return Bullet's damage.
     */
    int GetDamage() const { return m_Damage; }

    /**
     * @brief Get the author of the bullet.
     * @return true If bullet was fired by player.
     * @return false If bullet was fired by enemy.
     */
    bool FiredByPlayer() const { return m_FiredByPlayer; }

private:
    /**
     * Bullet's movement speed
     */
    int m_Speed;

    /**
     * Bullet's damage
     */
    int m_Damage;

    /**
     * Bullet's movement direction
     */
    int m_Direction;

    /**
     * Time when bullet made last move
     */
    chrono::time_point<chrono::steady_clock> m_LastMoved;

    /**
     * Bullet's author. True if author is player, false if author is enemy.
     */
    bool m_FiredByPlayer;
};


