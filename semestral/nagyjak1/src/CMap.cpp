#include "CMap.h"

void CMap::Display() const {
    clear();
    for (auto &i : m_Objects)
        i.second->Show();
    for (auto &i : m_Bullets)
        i->Show();
    for (auto &i : m_Bonuses)
        i.second->Show();
    if (m_Player != nullptr)
        m_Player->Show();
    if (m_Enemy != nullptr)
        m_Enemy->Show();
    refresh();
}

string CMap::SelectMap(const string &directory) {
    vector<string> files;
    int screenSizeX = getmaxx(stdscr);

    try {
        // load all valid map files into files vector
        for (const auto &file : std::filesystem::directory_iterator(directory.c_str())) {
            if (MapIsValid(file.path()))
                files.push_back(file.path());
        }
    }
    // if problem with directory or if no valid map file, return empty string
    catch (...) {
        return "";
    }
    if (files.empty())
        return "";

    bool first = true;
    int highlight = 0;
    while (true) {
        if (first)
            clear();
        first = false;
        box(stdscr, 0, 0);
        // print all file names from vector
        for (int i = 0; i < int(files.size()); ++i) {
            if (i == highlight)
                wattron (stdscr, A_REVERSE);
            mvwprintw(stdscr, i + 3, screenSizeX / 2 - int(files[i].length() / 2), files[i].c_str());
            wattroff (stdscr, A_REVERSE);
        }
        // handle movement through menu
        int ch = getch();
        switch (ch) {
            case KEY_UP:
                if (highlight != 0)
                    highlight--;
                break;
            case KEY_DOWN:
                if (highlight != int(files.size()) - 1)
                    highlight++;
                break;
            case ' ':
                return files[highlight];
            case KEY_RESIZE:
                first = true;
                break;
            default:
                break;
        }
    }
}

bool CMap::MapIsValid(const string &file) {
    set<pair<int, int>> walls;
    int players = 0;
    int enemies = 0;
    int x = 0, y = 0;
    ifstream f(file);

    if (f.is_open()) {
        int c;
        // go through all chars in a file
        while ((c = f.get()) != EOF) {
            if (c == '\n') {
                x = 0;
                y++;
                continue;
            }
            switch (c) {
                case 'W':
                    // store coordinates of walls for border check later
                    walls.insert(make_pair(x, y));
                    break;
                case 'E':
                    enemies++;
                    break;
                case 'P':
                    players++;
                    break;
                case 'R':
                case ' ':
                    break;
                // different char than W, E, P or R means wrong file
                default:
                    return false;
            }
            x++;
        }
        x--;
    // file can't be opened
    } else
        return false;
    // must have exactly 1 player, 1 enemy and walls must surround the map
    if (players != 1 || enemies != 1 || !MapWallsCheck(walls, x, y) || y > 35 || x > 120)
        return false;

    return true;
}

bool CMap::MapWallsCheck(const set<pair<int, int>> &walls, const int xSize, const int ySize) {
    for (int i = 0; i < xSize; ++i) {
        // line y=0 walls check
        if (walls.count(make_pair(i, 0)) == 0)
            return false;
        // line y=ySize walls check
        if (walls.count(make_pair(i, ySize)) == 0)
            return false;
    }
    for (int i = 0; i < ySize; ++i) {
        // line x=0 walls check
        if (walls.count(make_pair(0, i)) == 0)
            return false;
        // line x=xSize walls check
        if (walls.count(make_pair(xSize, i)) == 0)
            return false;
    }
    // if map is surrounded by walls return true
    return true;
}

void CMap::LoadMap(const string &path) {
    ifstream file(path);
    if (file.is_open()) {
        int x = 0, y = 0;
        int c;

        // go through file and load objects into map variables
        while ((c = file.get()) != EOF) {
            if (c == '\n') {
                y++;
                x = 0;
                continue;
            }
            switch (c) {
                case 'R':
                    m_Objects.insert(make_pair(make_pair(x, y), make_shared<CRock>(x, y, ROCK_HP)));
                    break;
                case 'W':
                    m_Objects.insert(make_pair(make_pair(x, y), make_shared<CWall>(x, y)));
                    break;
                case 'E':
                    m_Enemy = make_shared<CEnemy>(x, y, DEFAULT_SPEED, DEFAULT_BULLET_SPEED, DEFAULT_DAMAGE,
                                                  DEFAULT_ATTACK_SPEED, DEFAULT_HP, DEFAULT_ARMOR);
                    break;
                case 'P':
                    m_Player = make_shared<CPlayer>(x, y, DEFAULT_SPEED, DEFAULT_BULLET_SPEED, DEFAULT_DAMAGE,
                                                    DEFAULT_ATTACK_SPEED, DEFAULT_HP, DEFAULT_ARMOR);
                    break;
                default:
                    break;
            }
            x++;
        }
        m_Width = x - 1;
        m_Height = y;
        m_Original_Objects = m_Objects;
    }
}

void CMap::Reset() {
    m_Player->MakeStronger(0);
    m_Enemy->MakeStronger(0);
    m_Bonuses.clear();
    m_Bullets.clear();
    m_Boosts.clear();
    m_Objects = m_Original_Objects;
    for (auto &i : m_Objects)
        i.second->ResetHP();
}

void CMap::UpdateBonuses() {
    auto it = m_Bonuses.begin();
    // go through all bonuses
    while (it != m_Bonuses.end()) {
        it->second->Show();

        // check if player is standing on bonus
        if (m_Player->GetX() == it->first.first && m_Player->GetY() == it->first.second)
            // boost the player by the bonus
            it->second->Boost(m_Player, m_Boosts);

        // check if enemy is standing on bonus
        else if (m_Enemy->GetX() == it->first.first && m_Enemy->GetY() == it->first.second)
            // boost the enemy by the bonus
            it->second->Boost(m_Enemy, m_Boosts);

        if (it->second->Picked() || it->second->TimeOut()) {
            it->second->Hide();
            it = m_Bonuses.erase(it);
        } else
            ++it;
    }
}

void CMap::UpdateBoosts() {
    auto it = m_Boosts.begin();
    while (it != m_Boosts.end()) {
        // for every active boost in game check if it is valid
        if ((*it)->NotValid()) {
            (*it)->UnBoost();
            it = m_Boosts.erase(it);
        } else
            ++it;
    }
}

void CMap::UpdateBullets() {
    auto i = m_Bullets.begin();
    // go through all bullets in game
    while (i != m_Bullets.end()) {
        // move the bullet
        (*i)->Move();
        int damage = (*i)->GetDamage();
        auto bulletCords = make_pair((*i)->GetX(), (*i)->GetY());

        // check for collisions with objects
        auto it = m_Objects.find(bulletCords);
        if (it != m_Objects.end()) {

            // only CRock::TakeDamage() can return true when the rock is destroyed
            if (it->second->TakeDamage(damage)) {
                if (shared_ptr<CRock> rock = dynamic_pointer_cast<CRock>((*it).second)) {

                    // if rock is destroyed, chance to drop bonus on rocks position
                    rock->Drop(m_Bonuses);
                    // remove the rock from objects
                    m_Objects.erase(it);
                }
            }
            // collision happened, delete the bullet
            (*i)->Hide();
            m_Bullets.erase(i);
        }
        // check for collision with player
        else if (m_Player->GetX() == bulletCords.first && m_Player->GetY() == bulletCords.second) {
            // own bullets dont hurt, they go through the tank
            if ((*i)->FiredByPlayer())
                return;
            // collision happened, deal damage and delete the bullet
            m_Player->TakeDamage(damage);
            (*i)->Hide();
            m_Bullets.erase(i);
        }
        // check for collision with enemy
        else if (m_Enemy->GetX() == bulletCords.first && m_Enemy->GetY() == bulletCords.second) {
            // own bullets dont hurt, they go through the tank
            if (!(*i)->FiredByPlayer())
                return;
            // collision happened, deal damage and delete the bullet
            m_Enemy->TakeDamage(damage);
            (*i)->Hide();
            m_Bullets.erase(i);
        } else
            ++i;
    }
}

void CMap::Update() {
    // update all containers and reprint them
    UpdateBullets();
    UpdateBonuses();
    UpdateBoosts();
    for (auto &i : m_Objects)
        i.second->Show();
    m_Enemy->Show();
    m_Player->Show();
}