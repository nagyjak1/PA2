#pragma once

#include "CBonus.h"

/**
 * When picked up boosts tank's attack speed for certain time.
 * @brief Class represent attack speed bonus.
 */
class CAttackSpeedBonus : public CBonus {
public:
    /**
     * Construct bonus at given position, which upgrades tank's attack speed.
     * @brief Basic constructor.
     * @param x X-axis position
     * @param y Y-axis position
     */
    CAttackSpeedBonus(int x, int y);

    /**
     * @brief Upgrades tank's attack speed.
     * @param tank shared_ptr<CTank> Tank which will be boosted
     * @param boosts vector<shared_ptr<CBonus>> Vector to keep track of active bonuses
     */
    void Boost(shared_ptr<CTank> tank, vector<shared_ptr<CBonus>> &boosts) override;

    /**
     * Change the tank's attack speed to the state before the bonus was picked.
     * @brief End the effect of the boost.
     */
    void UnBoost() const override;
};


