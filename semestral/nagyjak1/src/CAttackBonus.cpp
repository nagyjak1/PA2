#include "CAttackBonus.h"

CAttackBonus::CAttackBonus(int x, int y)
        : CBonus(x, y) {
    m_DisplayChar = DAMAGE_BONUS;
}

void CAttackBonus::Boost(shared_ptr<CTank> tank, vector<shared_ptr<CBonus>> &boosts) {
    // tank can be boosted by only one bonus, remove previous boost before boosting with new one
    if (tank->Boosted())
        UnboostTank(tank, boosts);
    m_Picked = true;
    m_BoostedTank = tank;
    m_BoostTime = chrono::steady_clock::now();
    m_OriginalValue = tank->GetDamage();
    tank->GetDamage() *= DAMAGE_MULTIPLIER;
    boosts.push_back(make_shared<CAttackBonus>(*this));
    tank->Boost();
}

void CAttackBonus::UnBoost() const {
    m_BoostedTank->GetDamage() = m_OriginalValue;
    m_BoostedTank->Boost();
}