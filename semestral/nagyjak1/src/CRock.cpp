#include "CRock.h"

CRock::CRock(int x, int y, int health)
        : CObject(x, y),
          m_Health(health),
          m_OriginalHealth(health) {
    m_DisplayChar = ROCK;
}

bool CRock::TakeDamage(int damage) {
    m_Health -= damage;
    if (m_Health <= 0) {
        Hide();
        return true;
    }
    return false;
}

void CRock::Drop(map<pair<int, int>, shared_ptr<CBonus>> &bonuses) const {
    auto drop = random() % 2;
    if (drop == 1)
        return;
    auto bonusType = random() % 5;
    switch (bonusType) {
        case 0:
            bonuses.insert(make_pair(make_pair(m_X, m_Y), make_shared<CAttackSpeedBonus>(m_X, m_Y)));
            break;
        case 1:
            bonuses.insert(make_pair(make_pair(m_X, m_Y), make_shared<CAttackBonus>(m_X, m_Y)));
            break;
        case 2:
            bonuses.insert(make_pair(make_pair(m_X, m_Y), make_shared<CSpeedBonus>(m_X, m_Y)));
            break;
        case 3:
            bonuses.insert(make_pair(make_pair(m_X, m_Y), make_shared<CBulletSpeedBonus>(m_X, m_Y)));
            break;
        case 4:
            bonuses.insert(make_pair(make_pair(m_X, m_Y), make_shared<CHealthBonus>(m_X, m_Y)));
            break;
        default:
            break;
    }
}

void CRock::ResetHP() {
    m_Health = m_OriginalHealth;
}