#include "CMainMenuMode.h"

CMainMenuMode::CMainMenuMode()
        : m_Highlight(0),
          m_First(true) {
}

void CMainMenuMode::Display() {
    if (m_First)
        clear();
    box(stdscr, 0, 0);
    int xMax = getmaxx (stdscr);
    // print all choices
    for (auto i = 0; i < int(choices.size()); ++i) {
        // highlight the selected choice
        if (i == m_Highlight)
            wattron (stdscr, A_REVERSE);
        mvwprintw(stdscr, i + 3, xMax / 2 - int(choices[i].length() / 2), choices[i].c_str());
        wattroff (stdscr, A_REVERSE);
    }
    m_First = false;
}

int CMainMenuMode::HandleEvents() {
    while (true) {
        Display();
        int choice = getch();
        switch (choice) {
            case KEY_UP:
                if (m_Highlight != 0)
                    m_Highlight--;
                break;
            case KEY_DOWN:
                if (m_Highlight != choices.size() - 1)
                    m_Highlight++;
                break;
            case ' ':
                m_First = true;
                return m_Highlight;
            case KEY_RESIZE:
                m_First = true;
                break;
            default:
                break;
        }
    }
}