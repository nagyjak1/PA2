#pragma once

/*! \mainpage Tanks game
 *
 * \section intro_sec Introduction
 *
 * Tanks is an action game where you control your tank and your task is to destroy an enemy tank.
 *
 * Every time you kill the enemy, you move to another level, where the enemy is a bit stronger. Every second level,
 * you become equally strong with the enemy tank.
 *
 * Completing level increases your score by one.
 *
 * When your tank gets destroyed by enemy, you can enter your name and if your score is high enough, you will be displayed
 * on leaderboard, where top 10 players are shown.
 *
 * Before the game starts, you can choose a map, which you want to play on.
 *
 * ---------------------------------------------------------------------------------------------------------------------
 *
 * \section objects Objects
 * The map consists of two main objects:
 *
 * \subsection walls Walls '#'
 * * Walls can't be destroyed and will always surround the map
 *
 * \subsection rocks Rocks 'R'
 * * Rocks can be destroyed by bullets and will sometimes drop bonuses when destroyed.
 *
 * ---------------------------------------------------------------------------------------------------------------------
 *
 * \section bonuses Bonuses
 * Rocks can drop bonuses when destroyed. You can run over them and pick them up. You will get small boost for
 * a period of time. You can have only one active bonus at a time, if you are boosted by bonus and you pick a new
 * one, the old one will be disabled and you will get boosted by the new one. If the boost is not picked in time
 * it will disappear.
 * There are 5 types of bonuses:
 *
 * \subsection damage Damage bonus 'D'
 * * Your damage will be doubled.
 *
 * \subsection speed Speed bonus 'S'
 * * Your speed will be tripled.
 *
 * \subsection attackspeed Attack speed bonus 'A'
 * * Your attack speed will be doubled.
 *
 * \subsection bulletspeed Bullet speed bonus 'B'
 * * Your bullet's speed will be doubled.
 *
 * \subsection heal Heal bonus 'H'
 * * You will be healed by 20% of your current HP. If the heal heals you over your maximum health, the rest will be added to your armor.
 *
 * ---------------------------------------------------------------------------------------------------------------------
 *
 * \section menucontrols Controls in menu
 * \subsection basic Basic
 * * ARROW_UP - Move up
 * * ARROW_DOWN - Move down
 * * SPACE - Select
 *
 * ---------------------------------------------------------------------------------------------------------------------
 *
 * \section playcontrols Controls during playing
 *
 * \subsection movement Movement
 * * ARROW_UP - Move up
 * * ARROW_DOWN - Move down
 * * ARROW_LEFT - Move left
 * * ARROW_RIGHT - Move right
 *
 * \subsection firing Attack
 * * SPACE - Fire
 *
 * \subsection other Other
 * * P - Pause
 * * Q - Quit to main menu
 */

#include "CWelcomeMenu.h"
#include "CMainMenuMode.h"
#include "CLeaderboardMode.h"
#include "CPlayMode.h"
#include <memory>
#include <ncurses.h>
#include <iostream>

using namespace std;

/**
 * @brief Basic class to run the game
 */
class CGame {
public:
    /**
     * @brief Run the game
     */
    static void Run();

    /**
     * @brief Set screen settings for ncurses
     */
    static void ScreenSetup();
};