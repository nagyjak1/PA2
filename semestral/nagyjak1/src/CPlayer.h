#pragma once

#include "CTank.h"

/**
 * @brief Class representing tank controlled by user.
 */
class CPlayer : public CTank {
public:
    /**
     * @brief Basic constructor.
     * Constructs new player's tank with given parameters.
     * @param x X-axis position
     * @param y Y-axis position
     * @param speed Player's movement speed
     * @param bulletSpeed Player's bullet speed
     * @param damage Player's damage
     * @param attackSpeed Player's attack speed
     * @param health Player's health
     * @param armor Player's armor
     */
    CPlayer(int x, int y, int speed, int bulletSpeed, int damage, int attackSpeed, int health, int armor);

    /**
     * @brief Creates bullet fired by player and inserts it into given vector of bullets.
     * @param bullets vector<shared_ptr<CBullet>> & Vector of bullets from map to insert bullet into.
     */
    void Fire(vector<shared_ptr<CBullet>> &bullets);

    /**
     * Move tank according to keys pressed, fire bullet and handle special keys from the user (pause, quit, resize).
     * @brief Handles input from the user.
     * @param bullets Vector of bullets to insert into when tank fires
     * @return 1 If user wants to pause the game.
     * @return 2 If user wants to quit the game and go to main menu.
     * @return 3 If user wants to resize the window.
     * @return 0 Otherwise.
     */
    int GetMove(vector<shared_ptr<CBullet>> &bullets);
};


