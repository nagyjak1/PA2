#include "CPlayer.h"

CPlayer::CPlayer(int x, int y, int speed, int bulletSpeed, int damage, int attackSpeed, int health, int armor)
        : CTank(x, y, speed, bulletSpeed, damage, attackSpeed, health, armor) {
}

void CPlayer::Fire(vector<shared_ptr<CBullet>> &bullets) {
    // if time between firing is less than attackSpeed value, tank can't fire
    if (chrono::duration_cast<chrono::milliseconds>(chrono::steady_clock::now() - m_LastFired).count() <= m_AttackSpeed)
        return;
    m_LastFired = chrono::steady_clock::now();

    // create bullet in front of tank based on tank's direction
    int x, y;
    switch (m_Direction) {
        case UP:
            x = m_X;
            y = m_Y - 1;
            break;
        case DOWN:
            x = m_X;
            y = m_Y + 1;
            break;
        case LEFT:
            x = m_X - 1;
            y = m_Y;
            break;
        case RIGHT:
            x = m_X + 1;
            y = m_Y;
            break;
    }
    bullets.emplace_back(make_shared<CBullet>(x, y, m_BulletSpeed, m_Damage, m_Direction, true));
}

int CPlayer::GetMove(vector<shared_ptr<CBullet>> &bullets) {
    nodelay(stdscr, true);
    int choice = getch();
    switch (choice) {
        case KEY_UP:
            MoveUp();
            break;
        case KEY_DOWN:
            MoveDown();
            break;
        case KEY_LEFT:
            MoveLeft();
            break;
        case KEY_RIGHT:
            MoveRight();
            break;
        case ' ':
            Fire(bullets);
            break;
        case 'P':
        case 'p':
            return 1;
        case KEY_BACKSPACE:
            return 2;
        case KEY_RESIZE:
            return 3;
        default:
            break;
    }
    return 0;
}