#pragma once

#include <random>
#include "CTank.h"
#include "CPlayer.h"
#include "CBonus.h"
#include "map"

/**
 * @brief Class represents enemy tank controlled by enemy (computer).
 */
class CEnemy : public CTank {
public:
    /**
     * @brief Basic constructor.
     * Constructs new enemy's tank with given parameters.
     * @param x X-axis position
     * @param y Y-axis position
     * @param speed Enemy's movement speed
     * @param bulletSpeed Enemy's bullet speed
     * @param damage Enemy's damage
     * @param attackSpeed Enemy's attack speed
     * @param health Enemy's health
     * @param armor Enemy's armor
     */
    CEnemy(int x, int y, int speed, int bulletSpeed, int damage, int attackSpeed, int health, int armor);

    /**
     * @brief Decides what to do according to position of enemy.
     * @param bullets Vector of bullets for fired bullets
     * @param enemy Pointer to tank's enemy
     */
    void GetMove(vector<shared_ptr<CBullet>> &bullets, const shared_ptr<CTank> & enemy);

    /**
     * @brief Creates bullet fired by enemy and inserts it into given vector of bullets.
     * @param bullets vector<shared_ptr<CBullet>> & Vector of bullets from map to insert new fired bullet into.
     */
    void Fire(vector<shared_ptr<CBullet>> &bullets);

    /**
     * @brief Check if any bonus is located at given position.
     * @param y y-coordinates
     * @param x x-coordinates
     * @return true If bonus is located at given position.
     * @return false Otherwise.
     */
    static bool isBonus(int y, int x);

private:
    /**
     * Time of last move decision made by computer
     */
    chrono::time_point<chrono::steady_clock> m_LastDecision;
};


