#pragma once

#include <memory>
#include <map>
#include "CAttackBonus.h"
#include "CAttackSpeedBonus.h"
#include "CBulletSpeedBonus.h"
#include "CHealthBonus.h"
#include "CSpeedBonus.h"

/**
 * Class represents destroyable rock object. Rock can take damage from bullets. If rock is destroyed, there is a chance
 * to spawn a random type of bonus at rock's position.
 * @brief Class which represents rock - destroyable map object.
 */
class CRock : public CObject {
public:
    /**
     * @brief Basic constructor.
     * Constructs Rock object at given position.
     * @param x X-axis position
     * @param y Y-axis position
     * @param health Rock's HP
     */
    CRock(int x, int y, int health);

    /**
     * @brief Deals given damage to the rock.
     * @param damage Damage dealt
     * @return true If rock is destroyed.
     * @return false If rock is not destroyed.
     */
    bool TakeDamage(int damage) override;

    /**
     * @brief Set rock's HP to the state when loaded from file.
     */
    void ResetHP() override;

    /**
     * Function is called when rock is destroyed. Function may construct random bonus and insert it into map of bonuses.
     * @brief Chance to insert random bonus into bonuses map.
     * @param bonuses map<pair<int,int>,shared_ptr<CBonus>> Map of bonuses to insert the bonus into
     */
    void Drop(map<pair<int, int>, shared_ptr<CBonus>> &bonuses) const;

protected:
    /**
     * Rock's actual HP
     */
    int m_Health;

    /**
     * Rock's HP when generated from map file
     */
    int m_OriginalHealth;
};


