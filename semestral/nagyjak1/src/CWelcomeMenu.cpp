#include "CWelcomeMenu.h"

void CWelcomeMenu::Display() {
    clear();
    string message;
    int x, y;
    int xMax = getmaxx (stdscr);

    box(stdscr, 0, 0);
    message = "** TANKS **";
    mvprintw(3, xMax / 2 - int(message.length() / 2), message.c_str());

    getyx (stdscr, y, x);
    message = "< PRESS SPACE TO CONTINUE >";
    mvprintw(int(y + 2), xMax / 2 - int(message.length() / 2), message.c_str());
}

int CWelcomeMenu::HandleEvents() {
    int choice = getch();
    while (choice != ' ') {
        if (choice == KEY_RESIZE) {
            Display();
        }
        choice = getch();
    }
    return 3;
}