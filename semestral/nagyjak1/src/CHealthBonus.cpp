#include "CHealthBonus.h"

CHealthBonus::CHealthBonus(int x, int y)
        : CBonus(x, y) {
    m_DisplayChar = HP_BONUS;
}

void CHealthBonus::Boost(shared_ptr<CTank> tank, vector<shared_ptr<CBonus>> &boosts) {
    m_Picked = true;
    // health bonus is permanent
    m_BoostDuration = INT_MAX;
    m_BoostTime = chrono::steady_clock::now();
    int newHealth = tank->GetHealth() * HEALTH_MULTIPLIER;
    if (newHealth > tank->GetMaxHealth()) {
        tank->GetArmor() += newHealth - tank->GetMaxHealth();
        tank->GetHealth() = tank->GetMaxHealth();
    } else
        tank->GetHealth() = newHealth;
}

// health bonus can not be removed
void CHealthBonus::UnBoost() const {
}