#pragma once

#include "CLeaderboardMode.h"

using namespace std;

/**
 * @brief Class to display the main menu and to handle input from the user
 */
class CMainMenuMode {
public:
    /**
     * @brief Basic constructor
     */
    CMainMenuMode();

    /**
     * Displays the strings from the choices list
     * @brief Displays the menu
     */
    void Display();

    /**
     * Moves up and down in the menu using ARROWS KEY, handles RESIZE and SPACE to
     * @brief Handles input from the user
     * @return int if SPACE is pressed, returns index of selected option
     */
    int HandleEvents();

private:
    /**
     * Selected option in the menu
     */
    unsigned short m_Highlight;

    bool m_First;

    /**
     * List of options in menu
     */
    vector<string> choices = {"Start", "Leaderboard", "Quit"};
};


